import Card from "./components/Card";
import Feed from "./components/Feed";
import "./App.css";

export default function App() {
  return (
    <>
      <div className="App">
        <h1>Twitter</h1>
        <div
          style={{
            border: "1px solid black",
            padding: "10px",
          }}
        >
          <span>Escribir Tweet</span>
          <input type="text" />
        </div>

        {/* Renderizamos la lista */}
        <Feed />
        <Feed />
        <Feed />
      </div>
      <Card />
    </>
  );
}
