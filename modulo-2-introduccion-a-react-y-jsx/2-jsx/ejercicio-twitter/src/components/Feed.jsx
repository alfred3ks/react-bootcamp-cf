const Feed = () => {
  const listaDeTwets = [
    {
      id: 1,
      content: "Hola mundo",
    },
    {
      id: 2,
      content: "Hello word",
    },
  ];

  return (
    <ul>
      {listaDeTwets.map((tweet) => {
        return <li key={tweet.id}>{tweet.content}</li>;
      })}
    </ul>
  );
};

export default Feed;
