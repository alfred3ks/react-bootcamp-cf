import React, { Fragment } from "react";

// Tenemos un componente App:
export default function App() {
  return (
    <div className="App">
      <h1>Hola Mundo...</h1>
      <h2>Estamos usando ReactJS.</h2>
      <img
        // src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg"
        src="../src/img/logo.png"
        alt="React logo"
        height="100px"
      />
    </div>
  );
}
