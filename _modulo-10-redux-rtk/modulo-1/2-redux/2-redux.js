/*

Redux:

Tenemos diferentes componentes veamos como viaja la informacion: Ver pdf anexo en assets:
Asi seria el flujo que hace la informacion:

  View -> Action -> Reducer -> Store

De Store -> se le envia a View ver PDF de Redux en assets.

View -> son los componentes que creamos nosotros, las vistas.
Action -> son todas las acciones que hacen algo, lanzan objetos con claves que los recibe el Reducer, ejemplo: llamado a la BD, etc,
Reducer -> es el que recibe las claves del Action y actualiza el Store.

Las vistas no actualizan directamente la Store, existe todo un flujo de informacion. Como lo vemos con las view, los actions, los reducer y luego el store.

Redux es algo complejo pero muy escalable para proyectos muy grandes, recuerda para proyectos pequeños podemos usar context API.

Ventajas: de Redux, porque usar un manejador de estado:

- Una sola fuente de la verdad,
- La informacion puede ser accesada desde cualquier lugar,
- Persistencia del estado,
- Debuggear es muy sencillo, veremos una herramienta mas adelante para esto.
- Facil de testear.

*/