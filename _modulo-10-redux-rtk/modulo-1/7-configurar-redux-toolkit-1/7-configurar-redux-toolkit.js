/*
Configuracion de Redux toolkit:

Pasos para configurar store de Redux:

- Importar y crear reducer principal. El megaobjeto.
- Configurar un middleware para el trabajo asincrono, caracteristica de redux toolkit, por ej: llamado API.
- Configurar la extension de Redux DevTools. Para hacer debugger.

Vamos a instalar y configurar Redux ToolKit en el proyecto de recetas-react-redux.

Podemos ir tambien a la documentacion de Redux Toolkit para ver su documentacion:

https://redux-toolkit.js.org/

Tanto si es para un proyecto desde cero o como para implementarlo en un proyecto que ya esta funcionando.

https://redux-toolkit.js.org/tutorials/quick-start

Install Redux Toolkit and React-Redux
Add the Redux Toolkit and React-Redux packages to your project:

  npm install @reduxjs/toolkit react-redux

Lo siguiente segun la documentacion es crear la store de redux:

Create a Redux Store
Create a file named src/app/store.js. Import the configureStore API from Redux Toolkit. We'll start by creating an empty Redux store, and exporting it:

import { configureStore } from '@reduxjs/toolkit'

export const store = configureStore({
  reducer: {},
})

Nosotros en ves de crear la carpeta App crearemos la carpeta redux. Dentro del proyecto, dentro de src.

redux/actions
redux/reducer
redux/store

Pasamos a crear nuestro archivo para configurar el store.
redux/store/store.js

En el video lo llama index.js pero creo que es mejor llamarlo store.js es mas descriptivo.

Dentro del store.js metemos la informacion que nos dicen en la documentacion:

import { configureStore } from '@reduxjs/toolkit'

export const store = configureStore({
  reducer: {},
})

El profesor lo hace asi:

export default configureStore({
  reducer: {},
})






*/