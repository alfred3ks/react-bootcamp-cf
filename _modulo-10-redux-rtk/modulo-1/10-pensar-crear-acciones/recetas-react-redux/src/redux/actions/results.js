// Escribimos nuestras acciones: Declaramos las variables:
export const FETCH_RECIPES_START = 'FETCH_RECIPES_START';
export const FETCH_RECIPES_COMPLETE = 'FETCH_RECIPES_COMPLETE';
export const FETCH_RECIPES_ERROR = 'FETCH_RECIPES_ERROR';

// Asi declaramos nuestras acciones: Al arrancar
const fetchRecipesStart = () => {
  return {
    type: FETCH_RECIPES_START,
  }
}

// Accion cuando se ha completado: payload es un standart los dev de redux. es el argumento que traera la informacion de cada uno de los objetos ue trae la store:
const fetchRecipesComplete = (payload) => {
  return {
    type: FETCH_RECIPES_COMPLETE,
    payload,
  }
}

// const fetchRecipesError = (error) => {
//   return {
//     type: FETCH_RECIPES_ERROR,
//     error,
//   }
// }

// Asi nos ahorramos el return: Accion cuando hay un error:
const fetchRecipesError = (error) => ({
  type: FETCH_RECIPES_ERROR,
  error,
})