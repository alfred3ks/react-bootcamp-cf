/*

Ya sabemos como crear un reducer.

Es momento de pensar en acciones para poder dispararlas.

¿Como definir nuestras actions?

Las acciones son objetos planos. Diferenciados por un type. Pero... en base a que escribimos acciones.

Es lo que va a hacer esa accion o lo que quieres que haga en la store, o lo que quieres que haga por ti en caso de que un usuario interactue con la app. Imagina que de pronto quiees que te traiga por ejemplo las recetas entonces hay que ponerle un verbo antes a la accion, nuestro caso FETCH, o GET, CALL, etc. Como queramos, seguido con lo que va hacer. En este caso obtener recetas. Y luego el tipo de la accion que tiene que ser unico.

Vamos a crear nuestro primer archivo de acciones.
actions/results.js

Por convencion se suele hacer es llamar las actions igual que su reducer. Las acciones van a ser llamadas directamente por los componentes.

// Escribimos nuestras acciones:
export const FETCH_RECIPES_START = 'FETCH_RECIPES_START';
export const FETCH_RECIPES_COMPLETE = 'FETCH_RECIPES_COMPLETE';
export const FETCH_RECIPES_ERROR = 'FETCH_RECIPES_ERROR';

const fetchRecipesStart = () => {
  return {
    type: FETCH_RECIPES_START,
  }
}

const fetchRecipesComplete = (payload) => {
  return {
    type: FETCH_RECIPES_COMPLETE,
    payload,

  }
}

const fetchRecipesError = (error) => {
  return {
    type: FETCH_RECIPES_ERROR,
    error,
  }
}

Una ves definidas las acciones ahora las pasamos al reducer, en nuestro caso como se llaman igual reducers/results.js:

import { FETCH_RECIPES_START } from "../actions/results";
import { FETCH_RECIPES_COMPLETE } from "../actions/results";
import { FETCH_RECIPES_ERROR } from "../actions/results";


//Asi inicia nuestro reducer:
const initilState = {
  isLoading: false,
  data: [],
  error: {},
};

// ahora vamos a crear nuestro reducer.
const resultsReducer = (state = initilState, action) => {

  switch (action.type) {
    case FETCH_RECIPES_START:
      return {
        ...state,
        isLoading: true,
        data: [],
      }
    case FETCH_RECIPES_COMPLETE:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      }
    case FETCH_RECIPES_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      }

    default:
      return state;
  }
};

export default resultsReducer;

Ya tendriamos listas nuestras acciones y el reducer. Ahora veremos como disparar esas acciones desde pages/Index.jsx

Para disparar las acciones se suele usar el Dispatch() que veremos mas adelante.

*/