/*

Los pilares de Redux:
Ver pdf en assets:

Tenemos diferentes componentes veamos como viaja la informacion:

View -> Action -> Reducer -> Store

Vamos a ver los Reducer:

Reducer es la antesala que va a actualizar la Store. Antes de actualizar toda la informacion global del proyecto.

Reducers:
Son funciones (puras) que regresan una pieza (key) del estado global de la store dependiendo de la accion (type) que le sea enviado.

// Tenemos nuestro actions:
const ADD_TODO = 'ADD_TODO';

const addTodo = (text) => {
  type: ADD_TODO,
  text,
}

Asi luce un reducer:
// Reducer es una funcion en el cuerpo un switch:
const todoApp = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return{
        ...state,
        text:action.text,
      }
    default:
      return state
  }
}

Esta funcion recibe 2 argumentos, uno, es el state, que esta inicializado siempre o aun objeto vacio o a una ctte que se llama initialState, para que la store no inicio como undefined.

Como segundo argumento es el actions, es el action declarado antes arriba. Es un objeto. Recuerda eso.

---Ahora la Store: ---
Es la parte mas importante de nuestro proyecto porque es donde se va a concentrar toda la informacion.

Es un mega objeto que contiene todos los keys (reducers) con la informacion global que puede ser consumida por tus componentes.

Algo asi seria su apariencia:

{
  pockemons:{},
  user: {},
  locationInfo: {},
}

{
  user:{
    id: 'somefakeid',
    name 'John doe',
    age: 22,
  },
  pokemons: {
    data:[{...}],
  },
  userPreferences: {},
}

*/