// Esto es la buena practica colocarlos en una const con el type:
const FETCH_POKEMON_BY_NAME = 'FETCH_POKEMON_BY_NAME';

// Tenemos el Actions:
const fetchPokemonByName = (name) => {
  return {
    type: FETCH_POKEMON_BY_NAME,
    name,
  }
}

// Aqui tenemos nuestro reducer: Esta escuchando todas las acciones.
const pokemonsReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_POKEMON_BY_NAME:
      return {
        ...state,
        /* Lo que queremos modificar o agregar al store */
        pokemon: action.name,
      }
    default:
      return state;
  }
}
