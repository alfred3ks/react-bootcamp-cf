// Esto es la buena practica colocarlos en una const el valor del type:
export const FETCH_POKEMON_BY_NAME = 'FETCH_POKEMON_BY_NAME';

// Declaramos nuestro actions: Asi de simple escribimos una actions.
const fetchPokemonByName = (name) => {
  return {
    type: FETCH_POKEMON_BY_NAME,
    name,
  }
}

console.log(fetchPokemonByName('pikachu'));
/*
El resultado por consola:
{type: 'FETCH_POKEMON_BY_NAME', name: 'pikachu'}
*/


// Asi podemos evitarnos el return, construimos nuestro actions:
/*
const fetchPokemonByName = (name) => ({
    type: FETCH_POKEMON_BY_NAME,
    name,
})

*/

// En resumen esto es una accion. Regresa un objeto como vemos, un objeto plano. Construye un objeto.
