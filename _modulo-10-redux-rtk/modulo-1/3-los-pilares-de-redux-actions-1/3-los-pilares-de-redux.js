/*

Los pilares de Redux: Ver pdf en assets:

Tenemos diferentes componentes veamos como viaja la informacion:

View -> Action -> Reducer -> Store

Vamos a ver los Actions: Las acciones:

Son funciones (puras) que regresan un objeto plano {}, caracterizado por un unico tipo (type):

Objeto que regresa:
{
  type: ADD_TODO,
  text: 'Build my first Redux App'
}

// Aqui tenemos un actions de las dos formas de declarar una funcion:
// Funcion pura:
const ADD_TODO = 'ADD_TODO'; Esto es lo recomendable, declarar en una constante el valor del type, asi puede usarse en todo el proyecto.

function addTodo(text) {
  return {
    type: ADD_TODO,
    text,
  }
}

const addTodo = (text) => (
{
  type: ADD_TODO,
  text,
})

Cada una de las funciones que van a disparar un actions tienen un type, vease ese type como un id. Luego podemos pasarlo lo que queramos. El type define que lo que tenemos es un actions.

Vemos un ejemplo en la carpeta:
redux

*/