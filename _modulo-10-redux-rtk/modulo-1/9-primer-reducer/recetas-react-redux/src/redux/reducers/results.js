/*
Asi inicia nuestro reducer: es el estado inicial, es un objeto vacio, cuando arranque la pagina asi no sale undefined. Dentro de este objeto tenemos todas las key con que va a iniciar nuestro proyecto. Pero como se que son esos keys? vienen de aqui:

pages/Index.jsx:

  const [searchResults, setSearchResults] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

Lo que estamos haciendo es implementar Redux en este proyecto:

*/

const initilState = {
  isLoading: false,
  data: [],
  error: {},
};

// Ahora vamos a crear nuestro reducer. Falta crear el actions que lo haremos en la siguiente seccion:

const resultsReducer = (state = initilState, action) => {

  switch (action.type) {
    default:
      return state;
  }
};

export default resultsReducer;