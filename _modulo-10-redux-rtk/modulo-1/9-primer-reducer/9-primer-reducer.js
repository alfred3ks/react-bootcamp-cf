/*

Vamos ahora a crear nuestro primer reducer. Vamos a ver que combineReducers()

Dentro de la carpeta reducer vamos a crear nuestros primeros reducer.
Un archivo index.js que es el se encargara de exportar hacia la store, todos los reducer que vamos a crear.

Vamos a escribir nuestro primer reducers para obtener los resultados de las recetas.

Nuestro primer reducer se va a llamar results.js

// Asi inicia nuestro reducer:

const initilState = {
  isLoading: false,
  data: [],
  error: {},
};


// Ahora vamos a crear nuestro reducer.
const resultsReducer = (state = initilState, action) => {

  switch (action.type) {
    default:
      return state;
  }
};

export default resultsReducer;

Ahora en nuestro archivo reducer/index.js vamos a crear para exportar todos lo reducer que creemos, este archivo se le suele llamar el reducer root o reducer principal. aqui entra en juego:

  combineReducers():

import { combineReducers } from '@reduxjs/toolkit';
import resultsReducer from './results';

export default combineReducers({
  results: resultsReducer,
});

Ahora ese archivo reducer/index.js al store/store.js

import { configureStore } from '@reduxjs/toolkit';
import reducers from '../reducers';

export default configureStore({
  reducer: {
    reducer: reducers,
  },
})

NOTA: Como nombrar un reducers, lo haremos por lo que hace ese reducers, por lo que tiene esa keys.

*/