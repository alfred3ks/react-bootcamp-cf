/*

Redux RTK, Introduccion:

Juan del Toro.
@juan_deltoro2

Redux RTK es un manejador de estados. Es un manejador de estado muy popular dentro de la comunidad de React, es algo complejo de entender.

Existe otro manejador de estado llamado context API que ya vimos clases atras.

Existe otros manejadores de estados que aporta la comunidad. La idea es aprender las bases usando redux. Redux toolkit. Veremos tambien como son las bases para poder usar cualquier manejador de estado.

--- Historia de Redux:---
Ver pdf anexo en carpeta de assets:

Tenemos un arbol simulando los nodos de componentes html:

                                    nodoPadre
                              nodo1          nodo4
                          nodo2  nodo3     nodo5   nodo6


Antes de Redux lo que se hacia era manejar la informacion en cada uno de los componentes, esa informacion solo vivia dentro del propio del componente.

Por ejemplo tenemos la informacion del nodo6, esta solo vive ahi. Ahora veamos la informacion siempre fluye del padre a los hijos, si el nodoPadre tiene una informacion esta sera capas de mandarsela a todos los descendientes osea los nodos hijos. El nodoPadre puede pasar la informacion a todos los nodos descendientes. La informacion fluye de arriba hacia abajo.

Llega un punto en que el nodoPadre no puede contener toda la informacion de la aplicacion, porque es mucha informacion y muy pesada, por ejemplo que un componente este realizando 100 llamadas a la bd. Eso no es viable para un solo componente. Bueno pues hagamos que cada componenente maneja su informacion.

Ahora que sucede por decir que un nodo hijo tiene un tipo de informacion que deseamos que otro nodo hijo use esa informacion. Porque ambos tienen un objetivo similar, por ejemplo el nodo6 y el nodo2.

Como la informacion fluye hacia abajo este nodo6 no se la puede pasar al nodo4 y este luego al nodoPadre para que luego en cascada le llegue al nodo2. Aunque existen formas de hacerlo no es muy viable, lo mas interesante es conectar el nodo6 con el nodo2. Y lo mas simple no seria colocar la informacion en un solo lugar. Seria la fuente de toda la informacion de la cual queremos que accedan todos los nodos que la necesiten.

Asi nace redux.
Guardamos la informacion en un solo lugar y asi cualquier nodo puede acceder a esa informacion.

Ese sitio donde se guarda la informacion se le llama la store. Es un mega objeto que guarda la informacion y cualquier componente la puede usar. Esto es lo que llama un "manejador de estados". -> The state managment.

Cualquier componente puede ir y colocar la informacion en el state y cuando lo necesite cualquier otro componente simplemente este llama esa informacion.

Un manejador de estado es parte esencial de un proyecto, pero no siempre se necesita.

*/