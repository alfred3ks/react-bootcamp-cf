/*

Seguimos despues de la configuracion inicial de redux toolkit.

Ahora vamos a conectar la store con nuestro proyecto. Veremos como hacer que la store viva dentro de nuestros componentes. Osea que los componentes la puedan encontrar.

Tenemos que colocar un provedor que englobe toda la app. Al igual que se hizo con react router.

Es un componente:

Provide the Redux Store to React
Once the store is created, we can make it available to our React components by putting a React-Redux <Provider> around our application in src/index.js. Import the Redux store we just created, put a <Provider> around your <App>, and pass the store as a prop:

main.jsx:
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import { store } from './app/store/store'
import { Provider } from 'react-redux'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

Para este proyecto sera en el main.jsx. Nos quedaria asi:
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import './index.css';

import store from './redux/store/store';
import { Provider } from 'react-redux'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
)

    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>

Aqui vemos como envolvemos nuestras app con provider y le pasamos como propiedad una store.

Hasta aqui la configuracion basica, arrancamos nuestro proyecto y vemos que no tengamos errores en la consola y en la consola del navegador.

Seguimos...

*/