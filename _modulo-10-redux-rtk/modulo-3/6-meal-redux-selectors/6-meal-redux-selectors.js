/*

Redux selector:
Los selectores son basicamente funciones que aceptan el state de la store como argumento(o parte de ella) y regresa un valor especifico que necesitemos en determinado momento.

Como luce un selector:
Arrow function, direct lookup:
const selectEntities = state => state.entities;

Function declaration, mapping over an array to derive values:
function selectItemIds(state){
  return state.items.map(item => item.id);
}

Function declaration, encapsulation a deep lookup:
function selectSomeSpecificField(state){
  return state.some.deeply.nested.field;
}

Arrow function, deriving values from an array:
const selectItemsWhoseNamesStartWith = (items, namePrefix)=> items.filter(item=> item.name.startsWith(namePrefix))

Vamos a crear unos selectores.
No es necesario utilizar una dependencia en especifico para poder usarlos.

Existe una libreria que nos podria ayudar para calculos muy complejos, ahi si seria necesario usarlo.

Dentro de redux creamos nuestro selector:

redux/selectors.js

Lo podemos crear asi o dentro de su propia carpeta en este caso no pondremos la carpeta.

// Detail selector:
export const isLoadingDetail = (state) => state.detail.isLoading;
export const detailError = (state) => state.detail.error;
export const detailData = (state) => state.detail.data;

Ahora lo importamos en Meal.jsx:
import { isLoadingDetail, detailError, detailData } from "../redux/selectors";

Y luego los pasamos como coolback a useSelector():

  // Traemos las banderas desde la store usando el hooks useSelector:
  const meal = useSelector(detailData);
  const isLoading = useSelector(isLoadingDetail);
  const error = useSelector(detailError);

Me sigue dando fallo, me dice que data es undefined. nose lo veremos otro dia porque ahora no me va.

Esto se suele hacer asi para tener todo en un solo archivo. Esto son los selectores.

*/