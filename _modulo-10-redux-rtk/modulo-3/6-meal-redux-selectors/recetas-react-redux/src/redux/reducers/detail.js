// Meal.jsx:
import { FETCH_MEAL_DETAIL_START } from "../actions/detail";
import { FETCH_MEAL_DETAIL_COMPLETE } from "../actions/detail";
import { FETCH_MEAL_DETAIL_ERROR } from "../actions/detail";

const initialState = {
  isLoading: false,
  data: {},
  error: {},
}

const detailReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MEAL_DETAIL_START:
      return {
        ...state,
        isLoading: true,
        data: {},
      }
    case FETCH_MEAL_DETAIL_COMPLETE:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      }
    case FETCH_MEAL_DETAIL_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      }
    default:
      return state;
  }
}

export default detailReducer;
