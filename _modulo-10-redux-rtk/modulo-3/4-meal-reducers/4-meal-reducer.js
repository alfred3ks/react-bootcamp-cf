/*

Ahora vamos a crear nuestro reducers, recuerda que se debe llamar igual que nuestro actions.

redux/reducers/detail.js
Nos quedaria asi:

// Meal.jsx:
import { FETCH_MEAL_DETAIL_START } from "../actions/detail";
import { FETCH_MEAL_DETAIL_COMPLETE } from "../actions/detail";
import { FETCH_MEAL_DETAIL_ERROR } from "../actions/detail";

const initialState = {
  isLoading: false,
  error: null,
  data: {},
}

const detailReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MEAL_DETAIL_START:
      return {
        ...state,
        isLoading: true,
        data: {},
      }
    case FETCH_MEAL_DETAIL_COMPLETE:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      }
    case FETCH_MEAL_DETAIL_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      }
    default:
      return state;
  }

}

export default detailReducer;

Ahora debemos conectar este reducers con  uestro archivo maestro index, recordar eso sino no va a funcionar.


import detailReducer from './detail';

export default combineReducers({
  results: resultsReducer,
  details: detailReducer,
});

*/