/*

Vamos a ver la parte final de redux toolkit, Slice.

Redux Slice viene a simplificar todo lo que hemos visto hasta ahora en redux toolkit.

Un Slice es basicamente, actions y reducers compactados.

Podemos ver su documentacion:

https://redux-toolkit.js.org/api/createSlice

Veamos el ejemlo que muestra en la documentacion:

import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

interface CounterState {
  value: number
}

const initialState = { value: 0 } as CounterState

const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment(state) {
      state.value++
    },
    decrement(state) {
      state.value--
    },
    incrementByAmount(state, action: PayloadAction<number>) {
      state.value += action.payload
    },
  },
})

export const { increment, decrement, incrementByAmount } = counterSlice.actions
export default counterSlice.reducer

*/