import { createSlice } from "@reduxjs/toolkit";
import apiCall from "../../api";

const initialState = {
  isLoading: false,
  data: {},
  error: {},
}

const detailSlice = createSlice({

  name: 'mealDetail',
  initialState,
  reducers: {
    // Hacemos nuestras actions:
    fetchMealStart(state) {
      state.isLoading = true;
    },
    fetchMealError(state, action) {
      state.isLoading = false
      state.error = action.payload;

    },
    fetchMealComplete(state, action) {
      state.isLoading = false;
      state.data = action.payload;
    }
  }
})

// Trabajo asincrono conexion con API:
export const fetchMealDetail = (mealId) => async dispatch => {
  try {
    dispatch(fetchMealStart());
    const response = await apiCall(`/lookup.php?i=${mealId}`);
    dispatch(fetchMealComplete(response?.meals?.[0]));
  } catch (error) {
    dispatch(fetchMealError(error));
  }
}

export const isLoadingMealDetail = (state) => state.detailSlice.isLoading;
export const mealDetailData = (state) => state.detailSlice.data;
export const mealDetailError = (state) => state.detailSlice.error;

export const { fetchMealStart, fetchMealError, fetchMealComplete } = detailSlice.actions;
export default detailSlice.reducer;
