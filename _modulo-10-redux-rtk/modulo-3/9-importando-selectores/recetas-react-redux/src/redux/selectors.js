// Detail selector:
export const isLoadingDetail = (state) => state.detail.isLoading;
export const detailError = (state) => state.detail.error;
export const detailData = (state) => state.detail.data;