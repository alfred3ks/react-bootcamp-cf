/*

Ahora cada uno de los selectores que hemos creado en el apartado anterior los vamos a importar:

redux/slices/detail.js:
export const isLoadingMealDetail = (state) => state.detailSlice.isLoading;
export const mealDetailData = (state) => state.detailSlice.data;
export const mealDetailError = (state) => state.detailSlice.error;

Los importamos en pages/Meal.jsx:
import { fetchMealDetail, isLoadingMealDetail, mealDetailData, mealDetailError } from "../redux/slices/detail";

 // Traemos las banderas desde la store usando el hooks useSelector:
  const meal = useSelector(mealDetailData);
  const isLoading = useSelector(isLoadingMealDetail);
  const error = useSelector(mealDetailError);

A mi me sigue dando error. Lo tengo que mirar de nuevo porque el error.

*/