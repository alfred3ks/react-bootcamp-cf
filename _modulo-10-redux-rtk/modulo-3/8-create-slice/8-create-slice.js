/*

Vamos a ver la parte final de redux toolkit, Slice.

Ahora vamos a escribir un slice en el proyecto de recetas.

src/redux/slices:

Veremos como estara mas compactado como lo hemos comentado, todo en un solo archivo.

Importamos create slice desde redux toolkit:

import { createSlice } from "@reduxjs/toolkit";

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  data: {},
  error: {},
}

const detailSlice = createSlice({

  name: 'mealDetail',
  initialState,
  reducers: {
    // Hacemos nuestras actions:
    fetchMealStart(state) {
      state.isLoading = true;
      state.data = {};
    },
    fetchMealError(state, payload) {
      state.isLoading = false
      state.error = payload;

    },
    fetchMealComplete(state, payload) {
      state.isLoading = false;
      state.data = payload;
    }
  }

})

export const { fetchMealStar, fetchMealError, fetchMealComplete } = detailSlice.actions;
export default detailSlice.reducers;

Ahora ya este slice po exportamos a nuestro archivo index de reducer en reducers/index.js.

// Reducers:
import { combineReducers } from '@reduxjs/toolkit';
import resultsReducer from './results';
import detailReducer from './detail';

// Slices:
import detailSlices from '../slices/detail';


export default combineReducers({
  results: resultsReducer,
  detail: detailReducer,
  detailSlices,
});

// OJO el detail hace lo mismo que el detailSlice.

Todo ahora pasa mas compactado pero por detras de bambalinas pasa lo que hemos hecho en secciones anteriores, esto es como una capa de abstraccion. Todo parece mas simple.

Ahora el trabajo asincrono, vamos a ver como hacerlo.

// Trabajo asincrono conexion con API:
export const fetchMealDetail = (mealId) => async dispatch => {
  try {
    dispatch(fetchMealStart());
    const response = await apiCall(`/lookup.php?i=${mealId}`);
    dispatch(fetchMealComplete(response?.meals?.[0]));
  } catch (error) {
    dispatch(fetchMealError(error));
  }
}

Ahora vamos a ir al componente que dispara las acciones: Debemos es importar el slice ojo con eso que es el que acabamos de crear.

Importamos en en el componente Meal.jsx:
import { fetchMealDetail } from "../redux/slices/detail";

Aun asi me sigue dando error de data, lo ves por consola.
seguimos...

*/