/*
Aqui importamos todos nuestro reducer, esto es como un archivo maestro. Es el root reducer o reducer principal. Aqui entra en juego la funcion combineReducers():
*/

// Reducers:
import { combineReducers } from '@reduxjs/toolkit';
import resultsReducer from './results';
import detailReducer from './detail';

// Slices:
import detailSlices from '../slices/detail';


export default combineReducers({
  results: resultsReducer,
  detail: detailReducer,
  detailSlices,
});

// OJO el detail hace lo mismo que el detailSlice.

