/*

Ahora toca consumir la informacion, osea que nuestro componente renderice la informacion.
Recordar: pages/Meal.jsx:

Lo que haremos primero sera disparar las acciones usando dispatch, recordar que usaremos el hook useDispatch().

import { useDispatch } from 'react-redux';

Instanciamos el disparador:
const dispatch = useDispatch();

Ahora debemos disparar la accion creada en redux/actions/details/fetchMealDetail

import { fetchMealDetail } from "../redux/actions/detail";

Aqui disparamos la accion:
dispatch(fetchMealDetail(id));

Cuando hacemos la busqueda por ejemplo chicken y luego pulsamos sobre una de las recetas vamos a la herramientas del desarrollador y vemos con a extension de redux que se esta cargando esa receta. Dentro de la store ya tenemos dos keys, por un lado results, cuando cargamos toda la busqueda, y detail con el detalle de la receta cuando accedemos a una de ella.

Hasta aqui comprobado que funciona.

Ahora vamos a modificar el hook useEffect:
Vamos a quitar la funcion para el llamado a la api ya que aqui ya no la necesitamos solo dejaremos el diasparador. Esto fuera:

useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        setError({});
        setMeal({});
        const response = await apiCall(`/lookup.php?i=${id}`);
        setMeal(response?.meals?.[0]);
        // Aqui disparamos la accion:
        dispatch(fetchMealDetail(id));
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
}, [id]);

Ahora vemos que en nuestro componente tenemos mas variables en estado, eso ya lo vamos a quitar, porque eso para el componente esta bien pero necesitamos las variables del estado: esto fuera:

  const [meal, setMeal] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState({});

Vamos a traer esas banderas ahora desde el store:
Las importamos:
Para eso necesitamos un hook. useSelector, con este hooks le pasamos como collback el estado global para poder acceder a cada uno de los keys o banderas.

// Traemos las banderas desde la store usando el hooks useSelector:
const meal = useSelector((state) => state.detail.data);
const isLoading = useSelector((state) => state.detail.isLoading);
const error = useSelector((state) => state.detail.error);

Lo hago y me da error en la consola y al el no, lo he evisado y no veo donde esta la diferencia de el con el mio.

Seguimos...


*/