/*

Nos iremos a la carpeta redux del proyecto.

redux/actions/detail.js
redux/reducers/detail.js

El archivo detail.js es el encargado de hacer todos los action que seran de nuestro componente Meal.jsx.

Continuamos con la creacion de los actions en:

redux/actions/detail.js
Nos quedaria asi:

import apiCall from '../../api';

export const FETCH_MEAL_DETAIL_START = 'FETCH_MEAL_DETAIL_START';
export const FETCH_MEAL_DETAIL_COMPLETE = 'FETCH_MEAL_DETAIL_COMPLETE';
export const FETCH_MEAL_DETAIL_ERROR = 'FETCH_MEAL_DETAIL_ERROR';

const fetchMealDetailStart = () => ({
  type: FETCH_MEAL_DETAIL_START,
})

const fetchMealDetailComplete = (payload) => ({
  type: FETCH_MEAL_DETAIL_COMPLETE,
  payload,
})

const fetchMealDetailError = (error) => ({
  type: FETCH_MEAL_DETAIL_ERROR,
  error,
})

// Aqui vemos nuestro Creator Actions:
export const fetchMealDetail = (mealId) => dispatch => {
  try {
    // Esto viene de Meal.jsx:
    dispatch(fetchMealDetailStart());
    const response = await apiCall(`/lookup.php?i=${mealId}`);
    dispatch(fetchMealDetailComplete(response?.meals?.[0]));
  } catch (error) {
    dispatch(fetchMealDetailError(error));
  }
}

Ahora queda crear los reducers.


*/