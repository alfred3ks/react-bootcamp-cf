import { FETCH_RECIPES_START } from "../actions/results";
import { FETCH_RECIPES_COMPLETE } from "../actions/results";
import { FETCH_RECIPES_ERROR } from "../actions/results";
import { ADD_SEARCH_ITEM } from "../actions/results";

/*
Asi inicia nuestro reducer: es el estado inicial, es un objeto vacio, cuando arranque la pagina asi no sale undefined. Dentro de este objeto tenemos todas las key con que va a iniciar nuestro proyecto. Pero como se que son esos keys? vienen de aqui:

pages/Index.jsx:

  const [searchResults, setSearchResults] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

Lo que estamos haciendo es implementar Redux en este proyecto:
*/

// Iniciamos el estado:
const initilState = {
  isLoading: false,
  data: [],
  error: {},
  searchItems: [],
};

// Ahora vamos a crear nuestro reducer. Falta crear el actions que lo haremos en la siguiente seccion:

const resultsReducer = (state = initilState, action) => {
  console.log(state);
  switch (action.type) {
    case FETCH_RECIPES_START:
      return {
        ...state,
        isLoading: true,
        data: [],
      }
    case FETCH_RECIPES_COMPLETE:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      }
    case FETCH_RECIPES_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      }
    case ADD_SEARCH_ITEM:
      return {
        ...state,
        searchItems: [...state.searchItems, action.payload]
      }
    default:
      return state;
  }
};

export default resultsReducer;