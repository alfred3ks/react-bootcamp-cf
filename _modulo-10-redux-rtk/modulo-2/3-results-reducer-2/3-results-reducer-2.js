/*
Dispatch:

Esto es un trabajo sincrono, no lo haremos cuando se hace la llamada a la API: Esto es para ver cuando es una tarea SINCRONA.

Ahora vamos al reducer e importamos esa accion que hemos creado.
reducers/results.js

    case ADD_SEARCH_ITEM:
      return {
        ...state,
        item: action.payload
      }

Lo que haremos es nos guarde la accion en el store: Asi:
Modificamos nuestro initialState:

reducers/results.js:

const initilState = {
  isLoading: false,
  data: [],
  error: {},
  searchItems: [],
};

case ADD_SEARCH_ITEM:
  return {
    ...state,
    searchItems: [...state.searchItems, action.payload]
  }

Cuando se ejecute la accion cargara lo que ya hay en el state mas lo que traiga de action.payload.
Osea un nuevo items.

Cada vez que demos click al boton se actualiara el store. Lo vemos con un console.log():

const resultsReducer = (state = initilState, action) => {
  console.log(state);
  switch (action.type)


Ahora dentro del componente ejecutamos la accion:
pages/Index.jsx:

import { addSearchItem } from "../redux/actions/results";

const handleSearchClick = async () => {
  try {
    setIsLoading(true);
    // Aqui creamos el disparador al hacer click para generar la accion:
    dispatch(addSearchItem(searchText));

searchText es la constante o el valor del input. Lo vemos mas abajo:

<input
  type="text"
  placeholder="Buscar por nombre"
  className="text-lg p-1 border-2 rounded-sm	border-slate-500	w-96 h-full font-lato mt-2"
  value={searchText}

A hacer a primera busqueda vemos que la consola nos devuelve:

{isLoading: false, data: Array(0), error: {…}, searchItems: Array(0)}
data: []
error: {}
isLoading: false
searchItems: []
[[Prototype]]: Object

Esto es asi porque ahi donde esta el console.log() en ese punto esta vacio, ya que el action se ejecuta mas abajo, en la siguiente consulta o busqueda vemos como si se agrega el valor al arrays.

Al hacer varias busquedas vemos como se agregan al estado los valores del input:

{isLoading: false, data: Array(0), error: {…}, searchItems: Array(3)}
data: []
error: {}
isLoading: false
searchItems: (3) ['chicken', 'breack', 'meat']
[[Prototype]]: Object

Asi vemos como se disparan las acciones, con el dispatch y como se guardan en el store.

*/