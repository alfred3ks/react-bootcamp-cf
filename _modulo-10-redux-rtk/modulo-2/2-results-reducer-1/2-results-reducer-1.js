/*
Dispatch:

Ya hemos implementado nuestro disparador en nuestro archivo pages/Index.jsx

  // Implementamos useEffect y el disparador:
  useEffect(() => {
    // Vamos a disparar la accion:
    dispatch(
      fetchRecipesStart()
    );
  }, [])

Cuando se monte el componente se ejecutara la accion.

Vamos a comprobar si se esta ejecutando la accion vamos a nuestro reducer en reducers/results.jsx y lo vemos por un console.log():

const resultsReducer = (state = initilState, action) => {
  console.log('resultsReducers:', action);

Vemos la salida por consola: Funciona...
resultsReducers:
{type: 'FETCH_RECIPES_START'}
type: "FETCH_RECIPES_START"
[[Prototype]]: Object

Ahora lo que haremos es que se genere la accion al darle click al boton de buscar, no queremos que se cargue cada ves que montemos el componente, queremos que se ejecute la accion al hacer la busqueda asi que vamos a pages/Index.jsx:

  const handleSearchClick = async () => {
    try {
      setIsLoading(true);
      // Aqui creamos el disparador al hacer click para generar la accion:
      dispatch(fetchRecipesStart());

Vemos por consola que al hacer click se disparar el dispatch y ppr consecuencia la accion.

OJO cuando creamos acciones vemos por consola que se disparan unas acciones que nosotros no hemos creados, esas las ejecuta redux, para su funcionamiento interno. OJO:

resultsReducers: {type: '@@redux/INITd.q.7.i.2.6'}
results.js:27 resultsReducers: {type: '@@redux/PROBE_UNKNOWN_ACTIONt.p.h.e.t.l'}
results.js:27 resultsReducers: {type: '@@redux/INITd.q.7.i.2.6'}
results.js:27 resultsReducers: {type: '@@redux/PROBE_UNKNOWN_ACTIONa.0.s.d.a.j'}
results.js:27 resultsReducers: {type: '@@INIT'}

Ahora vemos que nos devuelve el caso cuando se hace click:

switch (action.type) {
    case FETCH_RECIPES_START:
      console.log('resultsReducers:', { ...state, isLoading: true, data: [] });
      return {
        ...state,
        isLoading: true,
        data: [],
      }

La consola:

resultsReducers:
{isLoading: true, data: Array(0), error: {…}}
data: []
error: {}
isLoading: true
[[Prototype]]: Object

Ya vemos como disparar acciones desde nuestro componente.

Ahora vamos a ver cuando se hace click en el boton se agreguen los datos al store. Vamos a action y creamos una nueva actions. actions/results.js

// Esta accion cuando se da click se guarda la informacion en la store:
export const addSearchItem = (payload) => ({
  type: ADD_SEARCH_ITEM,
  payload,
})

Esto es un trabajo sincrono, no lo haremos cuando se hace la llamada a la API: Esto es para ver cuando es una tarea SINCRONA.

Ahora vamos al reducer e importamos esa accion que hemos creado.
reducers/results.js

    case ADD_SEARCH_ITEM:
      return {
        ...state,
        item: action.payload
      }

Seguimos...

*/