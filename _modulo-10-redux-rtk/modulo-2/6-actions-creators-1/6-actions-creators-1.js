/*

Actions Creators: O creadores de acciones.

Son funciones que tienen el control de disparar acciones al recibir el dispatch como argumento(sincrono o asincrono).

Para crear los actions creator lo que se recomienda en el proyecto dentro de las actions.

actions/results.js

// Actions Creators:
export const fetchRecipes = async (text) => {
  try {
    console.log(10, text);
  } catch (error) {
    console.log(error);
  }
}

La importamos en nuestro componente, pages/Index.jsx:
import { fetchRecipes } from "../redux/actions/results";

  const handleSearchClick = async () => {
    try {
      setIsLoading(true);
      // Aqui creamos el disparador al hacer click para generar la accion:
      dispatch(addSearchItem(searchText));
      dispatch(fetchRecipes(searchText));

Aqui lo disparamos.
Al hacer la busqueda vemos la salida por consola:
10 'chicken'

Ahora vemos que ya podemos ahcer trabajo asincrono vamos hacer la llamada a la api usando este actions creator:

// Aqui vemos la llamada a la API:
      const response = await apiCall(`/search.php?s=${searchText}`);
      setSearchResults(response?.meals);
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }

Como vemos la apiCall es el que se encarga de hacer la llamada a la api, ahora lo vamos a hacer no en el componente pages/Index.jsx sino en el actions creators, actions/results.js:

import apiCall from '../api';

// Actions Creators:
// Llamada a la API: operacion asincrona:
export const fetchRecipes = async (text) => {
  try {
    const response = await apiCall(`/search.php?s=${text}`);
    console.log(response);
  } catch (error) {
    console.log(error);
  }
}

Hacemos la busqueda de recetas y vemos la salida por la consola.

*/