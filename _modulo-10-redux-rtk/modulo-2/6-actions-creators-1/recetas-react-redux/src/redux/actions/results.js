import apiCall from '../../api';

// Escribimos nuestras acciones: Declaramos las variables:
export const FETCH_RECIPES_START = 'FETCH_RECIPES_START';
export const FETCH_RECIPES_COMPLETE = 'FETCH_RECIPES_COMPLETE';
export const FETCH_RECIPES_ERROR = 'FETCH_RECIPES_ERROR';
export const ADD_SEARCH_ITEM = 'ADD_SEARCH_ITEM';


// Asi declaramos nuestras acciones: Al arrancar
export const fetchRecipesStart = () => {
  return {
    type: FETCH_RECIPES_START,
  }
}

// Accion cuando se ha completado: payload es un standart los dev de redux. es el argumento que traera la informacion de cada uno de los objetos ue trae la store:
export const fetchRecipesComplete = (payload) => {
  return {
    type: FETCH_RECIPES_COMPLETE,
    payload,
  }
}

// Asi nos ahorramos el return: Accion cuando hay un error:
export const fetchRecipesError = (error) => ({
  type: FETCH_RECIPES_ERROR,
  error,
})

// Actions Creators:
// Llamada a la API: operacion asincrona:
export const fetchRecipes = async (text) => {
  try {
    const response = await apiCall(`/search.php?s=${text}`);
    console.log(response);
  } catch (error) {
    console.log(error);
  }
}

// Esta accion cuando se da click se guarda la informacion en la store:
export const addSearchItem = (payload) => ({
  type: ADD_SEARCH_ITEM,
  payload,
})