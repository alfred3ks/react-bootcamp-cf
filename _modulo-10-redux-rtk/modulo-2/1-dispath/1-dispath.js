/*
Dispatch:

Diseñando nuestras acciones:

- Piensa en el nombre de tu accion primcipal, ese sera tu disparador que detonara mas acciones.
- El accion pricipal le avisara al reducer cuando el trabajo inicie, termine o si hubio un error(disparando mas acciones).

Ahora vamos a ver que es un disparador. The dispatch. Es aquel que que dispara desde los componentes, desde la vista.

A traves de los disparadores llamaremos las acciones, las acciones no se pueden llamar directamente desde el componente, para eso tenemos los dispatch.

- Prenderemos a usr el disparador de reduc(dispatch).
- Disparara las acciones desde la vista o componentes. El dispatch es un metodo de la store.

Para los dispatch tenemos un hook llamado useDispatch(). Vamos a su documentacion:

https://react-redux.js.org/api/hooks#usedispatch

Vemos lo que dice la documentacion con un ejemplo:

const dispatch = useDispatch()

import React from 'react'
import { useDispatch } from 'react-redux'

export const CounterComponent = ({ value }) => {
  const dispatch = useDispatch()

  return (
    <div>
      <span>{value}</span>
      <button onClick={() => dispatch({ type: 'increment-counter' })}>
        Increment counter
      </button>
    </div>
  )
}

Seguimos trabajando con el proyecto de recetas.

En el archivo actions/results.js hacemos exportables las tres funciones de las acciones.

Luego vamos a importar esas acciones en las vistas osea en los componentes:

pages/Index.jsx

Importamos useEffect y el dispatch y nuestra funcion actions:
import { useState, useEffect } from "react";
import { useDispatch } from 'react-redux';
import { fetchRecipesStart } from "../redux/actions/results";

  // Instanciamos useDispatch:
  const dispatch = useDispatch();
  // console.log(dispatch);

  // Implementamos useEffect y el disparador:
  useEffect(() => {
    // Vamos a disparar la accion:
    dispatch(
      fetchRecipesStart()
      );
  }, [])

Ahora vemos que cuando se monte el componente se ejecutara esta accion.

*/