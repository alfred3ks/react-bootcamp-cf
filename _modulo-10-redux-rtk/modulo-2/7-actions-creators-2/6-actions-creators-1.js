/*

Ahora vemos la funcion Actions creator que hemos creado, esta es una funcion que devuleve otra funcion asi:

export const fetchRecipes = (text) => async (dispatch) => {
  try {
    const response = await apiCall(`/search.php?s=${text}`);
    console.log(response);
  } catch (error) {
    console.log(error);
  }
}

Esto es lo mismo que vemos arriba:
function fetchRecipes(text) {
  return async function (dispatch) {
    Aqui ocurre el trabajo asincrono...
  }
}

Nuestro actions creators nos queda asi:

export const fetchRecipes = (text) => async (dispatch) => {
  try {
    console.log(dispatch);
    // ahora ya podemos disparar acciones aqui y no en la vista:
    dispatch(fetchRecipesStart());
    const response = await apiCall(`/search.php?s=${text}`);
    dispatch(fetchRecipesComplete(response?.meals))
    console.log(response);
  } catch (error) {
    console.log(error);
    dispatch(fetchRecipesError(error));
  }
}

De momento hasta aqui esto no se esta ejecuntando en el componente y producir el render de la informacion de la api, eso lo veremos mas adelante...


*/