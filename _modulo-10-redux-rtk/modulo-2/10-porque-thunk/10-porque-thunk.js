/*

¿Porque Thunk?

- Separa la lógica y trabajo de redux fuera de los componentes.
- Los reducers no pueden contener side effects y necesitaremos llamadas que traigan informacion de otro lugar.
- Nos permite escribir lógica que puede disparar más de 1 acción al mismo tiempo.
- Nos permite escribir lógica que sea capaz de acceder al estado global de la aplicacion.
- Redux toolkit lo trae preconfigurado.

*/