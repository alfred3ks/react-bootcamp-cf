/*

Redux middlewares:

Este concepto de middleware es algo que se maneja desde hace mucho tiempo y se puede ver en backend en nodeJS.

Tambien lo encontramos en otros lenguajes de programacion, php, etc.

Repasando el flujo en redux:

View -> Actions -> Reducer -> Store

De la Store -> View.

La vista dispara a traves de un dispatch la accion, el reducer y este se actualiza la store.

Ahora en que momento nos tenemos que preocupar con hacer por ejemplo una llamada a la API.

Por nuestro ejemplo vemos que tenemos una llamada a la API. Esa llamada se hace en el componente pages/Index.jsx, entonces que pasa en cada componente que necesite hacer una llamada hacemos el mismo codigo? eso no esta bien. Esa logica de codigo la sacamos de ahi.

Entonces en las vista no, en las acciones tampoco, tampoco en el reducer. ¿Entonces? Lo correcto es agregar un pequeño item, o un nuevo archivo de los middleware.

View -> Middeleware -> Actions -> Reducer -> Store.
De la Store -> View.

Los middeleware lo usaremos para este caso para el trabajo asincrono. Llamadas a la API, a la BD. Cualquier trabajo asincrono del proyecto. Tambien para hacer Logging, Crash Reports, etc.

Una definicion:
Un middleware es un punto entre el disparador de la accion y el momento en que llega al reducer.

Para iniciar con middleware necesitamos saber:

- applyMiddleware,
- Actions Creators,
- Redux-thunk, libreria para hacer trabajo asincrono.
- getState,
- dispatch.

*/