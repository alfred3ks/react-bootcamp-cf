/*

Redux thunk: Nos permite escribir funciones con lógica que pueda interactuar con la store y sus metodos (dispatch, getState).

Es la manera de ejecutar codigo asincrono en redux.

https://github.com/reduxjs/redux-thunk

Cuando se instala redux toolkit ya se instala redux thunk. Es mas ya lo hemos usado, esa funcion que se dispara en el action creator eso es redux thunk. Lo vemos en el ejemplo de recetas.

export const fetchRecipes = (text) => async (dispatch) => {
  try {
    console.log(dispatch);
    // ahora ya podemos disparar acciones aqui y no en la vista:
    dispatch(fetchRecipesStart());
    const response = await apiCall(`/search.php?s=${text}`);
    dispatch(fetchRecipesComplete(response?.meals))
    console.log(response);
  } catch (error) {
    console.log(error);
    dispatch(fetchRecipesError(error));
  }
}

async (dispatch)

Esto funciona porque ya viene preconfigurado al instalar redux toolkit.

*/