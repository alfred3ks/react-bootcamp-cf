/*

1. Introducción: 🔥
Veremos las diferentes formas de implementar CSS enfocado en React.js.

2. Listas: 🔥

Vamos a ver el tema de las listas en ReactJs, este tema es del modulo 3.

List:
Nos ayudan a renderizar un set de elementos con diferentes tipos de informacion:

Tenemos algunos metodos disponibles para las listas:

map()
filter()
forEach()
every()
some()

Los primeros dos son los mas utilizados. map(), filter().

Para entender las listas y como funciona en React tenemos que entender las llaves o keys:

Las llaves nos permiten identificar un elemento en el DOM de manera unica. Por lo tanto si si tenemos 5 elementos en un arreglo, la unica forma de que le indicamos a React.js de que un elemento es distinto del otro es a traves de esa propiedad key. Esta propiedad debe de ser unica y distinta de cada uno de los elementos de ese arreglo u objeto.

En algunos casos veremos que se usa el index de ese elemento en el array como key, pero eso no es muy recomendado hacerlo, porque ese valor puede variar si agregamos vas valores sobre todo al inicio del array y entonces esa referencia del key va a variar, y eso afecta porque se tiene que hacer todo el pre-rendering del componente y afecta en el rendimiento. Los key son identificadores unicos. Dos elementos no deben tener el mismo key.

A continuacion tenemos una lectura interesante sobre el tema.

https://robinpokorny.medium.com/index-as-a-key-is-an-anti-pattern-e0349aece318

Se puede dar el caso que tengamos un arrays de elementos, en este caso el key se suele utilizar en index del elemento como keys, lo cual no es muy recomendable, si el arrays se modifica react tiene que volver a asignar los keys y por ende un nuevo rendering de los todos los elementos y no es lo ideal.

3. Estilizando en React.js: 🔥

Existen varias formas de estilizar los componentes en React:

- Plain CSS,
- CSS Modules, -> Nativo de react.js
- CSS in JS, -> css en javascript.
- Preprocessors(SASS, PostCSS, LESS, Tailwind CSS), etc.

Dependera de cada proyecto y del equipo que trabaja con el.

Nosotros para esto vamos implentar CSS en JS:

- Styled Components,
- Emotion,
- JSS,
- Stitches, -> inspirada en style components.
- etc.

Usaremos Styled Components y Emotion, lo vemos a continuacion con el ejercicio de css.

4. Ejercicio de css con styled components y emotion: 🔥

El ejercicio lo encontramos en la carpeta 4-styled-components.

Para este ejercicio vamos a utilizar Styled Components y Emotion.

Tenemos el proyecto list-and-styles, ese es el repo de la clase pero yo me he creado el mio, listas-y-estilos.

Instalamos ambas dependecias a nuestro proyecto:
Styled Components y Emotions.

https://styled-components.com/
https://styled-components.com/docs/basics#installation

  npm install --save styled-components 🔥

https://emotion.sh/docs/introduction
https://emotion.sh/docs/install

  npm install --save @emotion/react 🔥

Ver la documentacion de ambos.

Vamos a ver como seria la explicacion de los que vamos haciendo en el ejemplo. 🔥

Tenemos el archivo: global.css. 🔥

Aqui ponemos los estilos globales de la aplicacion, como puede ser el reset del navegador y estilos que seran globales. Aqui podemos atacar al HTML y/o body. Este archivo lo llamamos donde carga la aplicacion que es el archivo App.jsx. Lo vemos a continuacion:

import "./components/global.css";

Un detalle que vamos a ver que se ha creado con componente general para los headinds del proyecto, ahi se definen todos ellos. Importamos a parte de styled components en la cabecera, css.

import styled, { css } from "styled-components";

Esto nos permite extender propiedades como lo vemos a continuacion:

// Aqui tenemos los estilos que son generales, con la propiedad de css que nos da styled components:
const BaseStyles = css`
  font-weight: 600;
`;

// Asi extendemos el componente:
export const HeadingH1 = styled.h1`
  ${BaseStyles}
  font-size: 24px;
  margin-bottom: 15px;
`;

Esto es muy interesante.

Una recomendación seria usar los style components de los componentes dentro de los mismos archivos del componente. Separarlos esta bien pero dependera de los grande que sea nuestro código para separar. Recuerda hacer un componentes que puedas usar en otros componentes de nuestra app. Como siempre depende....

Styled components que reciben props: 🔥

Los styled components pueden recibir props como lo vemos en el componente Comment.jsx:

Componente App.jsx: 🔥
import React from "react";
import "./components/global.css";
import { Container } from "./components/Container";
import { Input } from "./components/Input";
import { HeadingH1 } from "./components/Heading";
import styled, { css } from "styled-components";
import { Comment } from "./components/Comment";
import { comments } from "./data";

const GlobalStyle = css`
  margin-top: 32px;
`;

const App = () => {
  return (
    <div css={GlobalStyle}>
      <Container>
        <HeadingH1>Comments</HeadingH1>
        <Input />
        <CommentsBody>
          {comments.map((comment, id) => {
            return <Comment key={id} {...comment} />;
          })}
        </CommentsBody>
      </Container>
    </div>
  );
};

export default App;

// Aqui creamos un componente dentro del propio componente:
const CommentsBody = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

// Aqui creamos un componente dentro del propio componente:
const CommentsBody = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

Aqui vemos como enviamos las props:
<Comment key={id} {...comment} />;

Y las recibe en el component Comment.jsx🔥

import styled from "styled-components";
import { HeadingH4 } from "./Heading";

export const Comment = (props) => {
  const { name, body } = props; <- props
  return (
    <StyleCommentWrapper>
      <HeadingH4>{name}</HeadingH4>
      <CommentBody>{body}</CommentBody>
      <span>Replies</span>
    </StyleCommentWrapper>
  );
};

// Aqui hacemos el estilo:
const StyleCommentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 4px;
`;

const CommentBody = styled.p`
  line-height: 24px;
  color: #000;
`;

Tambien podemos dar estilos en linea como lo vemos en el componente Comment.jsx para el caso del span: 🔥

<span style={{ color: "#c3c3c3" }}>Replies</span>

Seguimos...

5 - Ejemplo usando emotion. 🔥

Vamos a ver como seria ahora montar el css con emotion.
Recordemos que debemos instalar la dependencia:

https://emotion.sh/docs/introduction
https://emotion.sh/docs/install

npm install --save @emotion/react 🔥

Emotion funciona mas o menos igual que la propiedad css de styled components.
Ver documentacion.

NOTA ADICIONAL: Como pasar por props y mostrar un color depende de una opcion u otra, lo vemos en el proyecto de 5-emotion.

data.js 🔥
export const comments = [
  {
    id: 1,
    name: 'John Doe',
    bio: 'Software Engineer learning about #web3',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dictum sem quis dictum. In sollicitudin lectus nec nisi suscipit fringilla. Morbi at efficitur erat.',
    flagged: false, 👑
    replies: [
      {
        id: 11,
        name: 'John Doe',
        bio: 'Software Engineer learning about #web3',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dictum sem quis dictum. In sollicitudin lectus nec nisi suscipit fringilla. Morbi at efficitur erat.',
      },
      {
        id: 12,
        name: 'John Doe',
        bio: 'Software Engineer learning about #web3',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dictum sem quis dictum. In sollicitudin lectus nec nisi suscipit fringilla. Morbi at efficitur erat.',
      },
      {
        id: 13,
        name: 'John Doe',
        bio: 'Software Engineer learning about #web3',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dictum sem quis dictum. In sollicitudin lectus nec nisi suscipit fringilla. Morbi at efficitur erat.',
      },
      {
        id: 14,
        name: 'John Doe',
        bio: 'Software Engineer learning about #web3',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dictum sem quis dictum. In sollicitudin lectus nec nisi suscipit fringilla. Morbi at efficitur erat.',
      },
    ]
  },
  {
    id: 1,
    name: 'Bruce Wayne',
    bio: 'I am not batman',
    body: 'Ciudad gótica es lo máximo!',
    flagged: true, 👑
    replies: []
  }
]

Componente App.jsx: 🔥
import React from "react";
import { Container } from "./components/Container";
import { Input } from "./components/Input";
import "./components/Global.css";
import { HeadingH1 } from "./components/Heading";
import styled, { css } from "styled-components";
import { Comment } from "./components/Comment";
import { comments } from "./data";

// Otra forma de inyectar css inline:
const GlobalStyles = css`
  margin-top: 32px;
`;

export const App = () => {
  return (
    <div className="App" css={GlobalStyles}>
      <Container>
        <HeadingH1>Comments</HeadingH1>
        <Input />
        <CommentsBody>
          {comments.map((comment, id) => {
            return <Comment key={id} {...comment} />;
          })}
        </CommentsBody>
      </Container>
    </div>
  );
};

const CommentsBody = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

Componente Comments.jsx: 🔥
import styled from "styled-components";
import { HeadingH4 } from "./Heading";

export const Comment = (props) => {
  console.log(props.flagged);
  return (
    // La bandera viene de data.js si es true pinta amarillo sino blanco:
    <StyleCommentWrapper flagged={props.flagged}>
      <HeadingH4>{props.name}</HeadingH4>
      <CommentBody>{props.body}</CommentBody>
      <span style={{ color: "#c3c3c3" }}>Replies</span>
    </StyleCommentWrapper>
  );
};

// Aqui hacemos el estilo: Tambien vemos que los style components tambien pueden recibir props.
const StyleCommentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px;
  background-color: ${(props) => {
    return props.flagged ? "yellow" : "white";
  }};
  gap: 4px;

  &:hover {
    background-color: #cccccc;
  }
`;

const CommentBody = styled.p`
  line-height: 24px;
  color: #000;
`;

Fin...

*/
