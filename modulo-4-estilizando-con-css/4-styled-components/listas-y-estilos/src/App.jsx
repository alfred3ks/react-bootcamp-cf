import React from "react";
import "./components/global.css";
import { Container } from "./components/Container";
import { Input } from "./components/Input";
import { HeadingH1 } from "./components/Heading";
import styled, { css } from "styled-components";
import { Comment } from "./components/Comment";
import { comments } from "./data";

const GlobalStyle = css`
  margin-top: 32px;
`;

const App = () => {
  return (
    <div css={GlobalStyle}>
      <Container>
        <HeadingH1>Comments</HeadingH1>
        <Input />
        <CommentsBody>
          {comments.map((comment, id) => {
            return <Comment key={id} {...comment} />;
          })}
        </CommentsBody>
      </Container>
    </div>
  );
};

export default App;

// Aqui creamos un componente dentro del propio componente:
const CommentsBody = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;
