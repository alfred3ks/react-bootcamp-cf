import React from "react";
import styled from "styled-components";
import { Button } from "./Button";

export const Input = () => {
  return (
    <StyledInputWrapper>
      <StyleInput placeholder="Insert your comment" />
      <StyleSeparator />
      <Button>Save comments</Button>
    </StyledInputWrapper>
  );
};

const StyledInputWrapper = styled.div`
  min-height: 20px;
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding: 16px;
  border: 1px solid #467ebf;
  border-radius: 6px;
`;

const StyleInput = styled.input`
  color: #000000;
  border: none;
  outline: none;
`;

const StyleSeparator = styled.hr`
  border: 1px solid #d7d7d7;
`;
