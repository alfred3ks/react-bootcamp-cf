// Para usar styled component debemos importarlo:
import styled from "styled-components";

// Asi creamos nuestro primer style component para un container:
export const Container = styled.div`
  width: 420px;
  min-height: 120px;
  display: flex;
  flex-direction: column;
  gap: 16px;
  padding: 24px;
  margin: 0 auto;
  /* margin-top: 32px; */
  background-color: #ffffff;
  border: solid 1px #cccccc;
  border-radius: 6px;
`;
