import styled from "styled-components";
import { HeadingH4 } from "./Heading";

export const Comment = (props) => {
  const { name, body } = props;
  return (
    <StyleCommentWrapper>
      <HeadingH4>{name}</HeadingH4>
      <CommentBody>{body}</CommentBody>
      <span style={{ color: "#c3c3c3" }}>Replies</span>
    </StyleCommentWrapper>
  );
};

// Aqui hacemos el estilo:
const StyleCommentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 4px;
`;

const CommentBody = styled.p`
  line-height: 24px;
  color: #000;
`;
