/*
4. Ejercicio de CSS - styled-components:

Para este ejercicio vamos a utilizar Styled Components y Emotion.

Tenemos el proyecto list-and-styles, ese es el repo de la clase pero yo me he creado el mio, listas-y-estilos.

Instalamos ambas dependecias a nuestro proyecto:
Styled Components y Emotions.

https://styled-components.com/
https://styled-components.com/docs/basics#installation

  npm install --save styled-components 🔥

https://emotion.sh/docs/introduction
https://emotion.sh/docs/install

  npm install --save @emotion/react 🔥

Ver la documentacion de ambos.

Vamos a ver como seria la explicacion de los que vamos haciendo en el ejemplo. 🔥

Tenemos el archivo: global.css. 🔥

Aqui ponemos los estilos globales de la aplicacion, como puede ser el reset del navegador y estilos que seran globales. Aqui podemos atacar al HTML y/o body. Este archivo lo llamamos donde carga la aplicacion que es el archivo App.jsx. Lo vemos a continuacion:

import "./components/global.css";

Un detalle que vamos a ver que se ha creado con componente general para los headinds del proyecto, ahi se definen todos ellos. Importamos a parte de styled components en la cabecera, css.

import styled, { css } from "styled-components";

Esto nos permite extender propiedades como lo vemos a continuacion:

// Aqui tenemos los estilos que son generales, con la propiedad de css que nos da styled components:
const BaseStyles = css`
  font-weight: 600;
`;

// Asi extendemos el componente:
export const HeadingH1 = styled.h1`
  ${BaseStyles}
  font-size: 24px;
  margin-bottom: 15px;
`;

Esto es muy interesante.

Una recomendación seria usar los style components de los componentes dentro de los mismos archivos del componente. Separarlos esta bien pero dependera de los grande que sea nuestro código para separar. Recuerda hacer un componentes que puedas usar en otros componentes de nuestra app. Como siempre depende....

Styled components que reciben props: 🔥

Los styled components pueden recibir props como lo vemos en el componente Comment.jsx:

Componente App.jsx: 🔥
import React from "react";
import "./components/global.css";
import { Container } from "./components/Container";
import { Input } from "./components/Input";
import { HeadingH1 } from "./components/Heading";
import styled, { css } from "styled-components";
import { Comment } from "./components/Comment";
import { comments } from "./data";

const GlobalStyle = css`
  margin-top: 32px;
`;

const App = () => {
  return (
    <div css={GlobalStyle}>
      <Container>
        <HeadingH1>Comments</HeadingH1>
        <Input />
        <CommentsBody>
          {comments.map((comment, id) => {
            return <Comment key={id} {...comment} />;
          })}
        </CommentsBody>
      </Container>
    </div>
  );
};

export default App;

// Aqui creamos un componente dentro del propio componente:
const CommentsBody = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

// Aqui creamos un componente dentro del propio componente:
const CommentsBody = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

Aqui vemos como enviamos las props:
<Comment key={id} {...comment} />;

Y las recibe en el component Comment.jsx🔥

import styled from "styled-components";
import { HeadingH4 } from "./Heading";

export const Comment = (props) => {
  const { name, body } = props; <- props
  return (
    <StyleCommentWrapper>
      <HeadingH4>{name}</HeadingH4>
      <CommentBody>{body}</CommentBody>
      <span>Replies</span>
    </StyleCommentWrapper>
  );
};

// Aqui hacemos el estilo:
const StyleCommentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 4px;
`;

const CommentBody = styled.p`
  line-height: 24px;
  color: #000;
`;

Tambien podemos dar estilos en linea como lo vemos en el componente Comment.jsx para el caso del span: 🔥

<span style={{ color: "#c3c3c3" }}>Replies</span>

Tambien podemos inyectar estilos en linea usando la propiedad css de styled components, lo vemos en el componente App.jsx:



Seguimos...


*/
