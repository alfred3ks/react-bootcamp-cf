import React from 'react'

const ProfilePicture = () => {
  return (
    <div className='profile-picture'>
      <img src="https://avatars3.githubusercontent.com/u/17098477?s=460&v=4" alt="profile-picture" />
    </div>
  )
}

export default ProfilePicture;