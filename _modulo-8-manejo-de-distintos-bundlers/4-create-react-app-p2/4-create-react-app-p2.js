/*

Create React App: Es un bundle.

Ventajas:
- Zero Config.
- No necesita paquetes extra.
- Facil de mantener.
- Facil de usar.
- Permite eyectar en caso de requerir configuracion de webpack.

Desventajas:
- No se puede configurar builds personalizadas.
- Todo esta abstracto.
- Viene con SASS configurado (Aunque no lo usemos).

Ahora sobre nuestro ejemplo vamos a ejecutar:

npm run build

Se nos creara una carpeta build que es la que debemos llevar a produccion.

CRA es una envoltura de webpack, lo que esta por debajo es webpack.

Ahora si quisieramos quitar esa envoltura para hacer mas configurable nuestro proyecto sobre todo para grandes proyectos para eso tenemos el comando:

npm run eject

Tendremos un proyecto que ya podremos configurar a nuestro gusto en funcion de nuestras necesidades.

*/