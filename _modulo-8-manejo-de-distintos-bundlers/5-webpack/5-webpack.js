/*

Webpack: Es un buldle que nos permite empaquetar todos los elementos de nuestro proyecto para tener una sola salida.

Es lo que tenemos detras de CRA. Create React App.

Ahora vamos a trabajar directamente con webpack. Vamos a ver unas configuraciones que vamos a repasar.

Existe un archivo:
webpack.config.js

module.exports = {
  entry: './path/to/my/entry/file.js',
  output:{
    path: path.resolv(--dirname, 'dist'),
    filename: 'my-first-webpack.bundle.js'
  }
};

Le decimos de donde viene el archivo principal del proyecto o sea el punto de entrada del mismo, y la salida para el bundle para produccion.

Ademas otra caracteristica de la configuracion tenemos los loaders, cada archivo que no sea .js, llamase .sass, .png, webpack no sabe como procesarlo, entonces para eso tenemos los loaders.

module.exports = {
  entry: './path/to/my/entry/file.js',
  output:{
    path: path.resolv(--dirname, 'dist'),
    filename: 'my-first-webpack.bundle.js'
  },
  module: {
    rules:[{test: /\.txt$/, use: 'raw-loader'}],
  },
};

Lo vemos en la key module. Con unas reglas o rules.

Tambien vienen los pluging, con cra no podemos usar plugins.

module.exports = {
  entry: './path/to/my/entry/file.js',
  output:{
    path: path.resolv(--dirname, 'dist'),
    filename: 'my-first-webpack.bundle.js'
  },
  module: {
    rules:[{test: /\.txt$/, use: 'raw-loader'}],
  },
  plugins: [new HtmlWebpackPlugin({template: './src/index.html'})],
};

Tambien el mode:

module.exports = {
  mode: 'production',
}

Esta es la configuracion mas basica de webpack. En funcion de la necesidad esta ira creciendo.
Ahora vamos a ver como configurar un proyecto con web pack.


*/