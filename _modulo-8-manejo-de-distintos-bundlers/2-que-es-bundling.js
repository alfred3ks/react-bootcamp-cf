/*

Te preguntaras:

¿Como es que todos nuestros mudulos son unificados a javascript para poder tener un solo archivo final?

Cuando tenemos un archivo html que usa varios archivos js tendriamos esos script al final del body de html, eso cuando tenemos una aplicacion con vanila js:

HTML  -> 1.js
      -> 2.js

etc.

Algo asi:

  <body>

  ...

  <script src="api.js"></script>
  <script src="auth.js"></script>
  <script src="libs/react.min.js"></script>
  <script src="rickasley.js"></script>

  </body>

Ahora que sucede con un proyecto de react con multiples componentes. Es imposible que cada uno de los componentes creados para la app se vayan y se anclen de la misma manera que el ejemplo anterior al final del body del html.

Seria no viable.

Es por eso que vienen lo bundle, es basicamente a aquel proceso de unificar cada uno de los archivos de javascript en uno solo. Todos los archivos, los componetes de JS, dependencias, se empaquetan y salen en una sola salida.

Un bundling es la herramienta que hace un bundle. Llamese create react app, webpack, o vite.

Asi es como nos quedaria:

  <body>
  ...

  <script src="dist/bundle.js"></script>

  </body>


*/