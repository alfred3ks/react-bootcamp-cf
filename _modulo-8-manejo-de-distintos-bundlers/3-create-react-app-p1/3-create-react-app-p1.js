/*

Create React App: Es un bundle.

¿Que es?

Es la forma mas facil para crear una aplicacion de React JS sin la necesidad de realizar configuraciones, es decir el tema de configuraciones para el bundling no estan disponibles para este paquete, por lo cual no es la mejor utileria para un proyecto enterprise a gran escala, debido a que tiene limitaciones que podrian ayudarnos a mejorar el performance de nuestro proyecto.

https://create-react-app.dev

Vamos a crear una app sencilla usando esta herramienta.

npx create-react-app my-first-app

*/