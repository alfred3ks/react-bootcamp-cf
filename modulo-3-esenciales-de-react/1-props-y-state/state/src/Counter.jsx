import { useState } from "react";
// Aqui implementamos el hooks de estado:
const Counter = () => {
  const [score, setScore] = useState(0);
  const [hover, setHover] = useState(false);

  // Esto tambien cambiamos el estado de css con el hook:
  let className = "counter";
  if (hover) {
    className += " hover";
  }

  return (
    <div
      className={className}
      onPointerEnter={() => setHover(true)}
      onPointerLeave={() => setHover(false)}
    >
      <h1>{score}</h1>
      <button onClick={() => setScore(score + 1)}>Add +</button>
    </div>
  );
};

export default Counter;
