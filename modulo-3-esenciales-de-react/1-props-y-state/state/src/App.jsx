import Counter from "./Counter";
import Agregar from "./Agregar";
import "../src/css/styles.css";

// Componente App:
const App = () => {
  return (
    <div>
      <div className="flex">
        <Counter />
        <Counter />
      </div>
      <Agregar />
    </div>
  );
};

export default App;
