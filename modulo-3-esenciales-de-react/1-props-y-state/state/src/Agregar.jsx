import { useState } from "react";
// Me creo un componente para agregar a una lista otro elemento:
const Agregar = () => {
  const [hover, setHover] = useState(false);
  const [fruits, setFruits] = useState([
    {
      id: 1,
      text: "Manzana",
    },
    {
      id: 2,
      text: "Pera",
    },
    {
      id: 3,
      text: "Uva",
    },
  ]);

  // Esto tambien cambiamos el estado de css con el hook:
  let className = "counter";
  if (hover) {
    className += " hover";
  }

  return (
    <div className="list">
      <div
        className={className}
        onPointerEnter={() => setHover(true)}
        onPointerLeave={() => setHover(false)}
      >
        <h1>Listado de frutas</h1>
        <button
          onClick={() => {
            // Asi garantizamos no mutar el array original:
            const copiaArray = [...fruits, { id: 4, text: "Durazno" }];
            setFruits(copiaArray);
          }}
        >
          Add +
        </button>
      </div>
      <ul>
        {fruits.map((fruit) => {
          return <li key={fruit.id}>{fruit.text}</li>;
        })}
      </ul>
    </div>
  );
};

export default Agregar;
