import getImageUrl from "../utils";
import PropTypes from "prop-types";

export default function Avatar(props) {
  return <Perfil {...props} />;
}

function Perfil({ person, size, text = "default" }) {
  return (
    <>
      <h2>{text}</h2>
      <img
        className="avatar"
        src={getImageUrl(person)}
        alt={person.name}
        width={size}
        height={size}
      />
    </>
  );
}

// Aqui vemos como podemos hacer que el componente reciba las props con sus tipos:
Avatar.propTypes = {
  person: PropTypes.shape({
    name: PropTypes.string.isRequired,
    imageId: PropTypes.string.isRequired,
  }).isRequired,
  size: PropTypes.number.isRequired,
  text: PropTypes.string,
};
