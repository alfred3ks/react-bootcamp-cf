import Avatar from "./Avatar";

export default function Profile() {
  let katsuko = { name: "Katsuko Saruhashi", imageId: "YfeOqp2" };
  return (
    <div>
      <Avatar size={100} person={katsuko} text="Soy un props opcional" />
      <Avatar
        size={80}
        person={{
          name: "Aklilu Lemma",
          imageId: "OKS67lh",
        }}
      />
      <Avatar
        size={50}
        person={{
          name: "Lin Lanying",
          imageId: "1bX5QH6",
        }}
      />
    </div>
  );
}
