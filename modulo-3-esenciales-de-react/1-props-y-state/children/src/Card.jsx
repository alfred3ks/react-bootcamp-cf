// Componente que recibe un hijo por props:
const Card = ({ children }) => {
  return <div className="card">{children}</div>;
};

export default Card;
