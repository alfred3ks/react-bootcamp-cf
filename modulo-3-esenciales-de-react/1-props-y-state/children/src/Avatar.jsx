import getImageUrl from "../utils";
import PropTypes from "prop-types";

export default function Avatar(props) {
  return <Perfil {...props} />;
}

// Aqui vemos como podemos hacer que el componente reciba las props con sus tipos:
Avatar.propTypes = {
  person: PropTypes.shape({
    name: PropTypes.string.isRequired,
    imageId: PropTypes.string.isRequired,
  }).isRequired,
  size: PropTypes.number.isRequired,
};

function Perfil({ person, size }) {
  return (
    <img
      className="avatar"
      src={getImageUrl(person)}
      alt={person.name}
      width={size}
      height={size}
    />
  );
}
