import Avatar from "./Avatar";
import Card from "./Card";

export default function Profile() {
  return (
    <Card>
      <Avatar
        size={100}
        person={{
          name: "Katsuko Saruhashi",
          imageId: "YfeOqp2",
        }}
      />
      {/* Renderizamos el componente ejemplo con spread operator */}
      <Ejemplo
        size={80}
        person={{
          name: "Katsuko Saruhashi",
          imageId: "YfeOqp2",
        }}
      />
    </Card>
  );
}

// Aqui vemos un componente que recibe muchas props: Como propagarlas con spread operator:
function Ejemplo(props) {
  return <Avatar {...props} />;
}
