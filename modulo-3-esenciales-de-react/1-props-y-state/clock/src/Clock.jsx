import React from "react";

// Vemos la sintaxis de Object destructuring:
const Clock = ({ color, time }) => {
  return <h1 style={{ color: color }}>{time}</h1>;
};

export default Clock;
