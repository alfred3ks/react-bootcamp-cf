import React from "react";
// Importamos este hooks llamado useRefs:
import { useRef } from "react";

/*
Con el hook ref podemos usar metodos y propiedades del DOM, como este caso para el focus del input. Recuerda que react trabaja con virtual dom.
Las referencias solo las debemos usar cuando un comportamiento react no lo proporcione.
Para el caso del input y del focus react no lo proporciona. Eso seria un antipatron.

OJO:

Usaremos una referencia solo cuando necesitemos de forma imperativa llamar a una funcion para dar un comportamiento que React no nos proporcione.

La referencia nos devuelve un objeto con un atributo llamado current. Lo vemos en el console.log de la funcion handleFocusInput.

const handleFocusInput = () => {
    console.log(inputRef); <- AQUI:
  };

Esa propiedad current puede ser null o el elemento de HTML. Par nuestro caso tenemos el input.
Ahora ya podemos acceder a todo lo que nos provee el DOM y que no nos puede dar react.js. Osea con ref ya podemos acceder a toda la API del DOM para ese elemento.

*/

const ExampleRefs = () => {
  const inputRef = useRef(null);

  const handleFocusInput = () => {
    console.log(inputRef);

    if (inputRef) {
      // Si el elemento existe en el DOM has el focus.
      inputRef.current.focus();
      // Vemos como podemos acceder a todas las propiedades del DOM y alterarlo.
      inputRef.current.style.background = "crimson";
      inputRef.current.style.color = "#fff";
    }
  };

  return (
    <>
      <p>Example Refs</p>
      <input ref={inputRef} type="text" />
      <button onClick={handleFocusInput}>Focus Input</button>
    </>
  );
};

export default ExampleRefs;
