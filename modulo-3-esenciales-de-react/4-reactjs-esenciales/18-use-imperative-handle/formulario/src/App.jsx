import React from "react";
import { useRef } from "react";
import { Slide, Slider } from "./Slider";

export const App = () => {
  // Vamos a generar la referencia:
  const sliderRef = useRef(null);

  const handleSlider = (direction) => {
    console.log(direction);
    console.log(sliderRef);

    if (direction === "next") {
      sliderRef.current.next();
    } else {
      sliderRef.current.prev();
    }
  };

  return (
    <>
      <div
        style={{
          marginTop: 20,
          marginBottom: 20,
          background: "crimson",
          color: "white",
          height: "100px",
        }}
      >
        <Slider
          ref={sliderRef}
          // Este objeto de opciones lo vemos en la documentacion del slide:
          options={{
            slides: {
              perWiew: 1,
            },
          }}
        >
          <Slide>1</Slide>
          <Slide>2</Slide>
          <Slide>3</Slide>
        </Slider>
      </div>
      <button onClick={() => handleSlider("prev")}>Previous</button>
      <button onClick={() => handleSlider("next")}>Next</button>
    </>
  );
};
