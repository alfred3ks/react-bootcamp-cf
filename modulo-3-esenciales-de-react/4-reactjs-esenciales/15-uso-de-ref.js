/*
15 - Uso de ref:

Cuando no usar Ref:
Para simular comportamientos de componentes controlados (anti-patterns).

Ref:Referencias:
Las referencias o mejor conocidad dentro de React como Refs, nos permiten acceder a propiedades del elemento dentro del DOM directamente.

Solo usamos refs cuando necesitemos implicitamente llamar a un comportamiento que React no nos permita controlar.

- Integrar una libreria de terceros,
- Gestionar focus, text selection, etc.
- Detectar si el elemento existe o no.

*/
