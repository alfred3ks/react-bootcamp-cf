/*
17 - ForwarsRef:

forwardRef: Referencias de reversa o hacia atras.

Esta tecnica la vamos a utilizar cuando nos vemos en la necesidad se aislar funcionalidad para evitar que nuestro codigo crezca mucho.
Esto significa que nos permitira no perder la referencia de un elemento aunque este en otro componente.

Imaginemos que tenemos un archivo de React con 500 lineas. Algo muy grande porque esos componentes requieren de toda esas lineas de codigo.

Ahi nos surge la necesidad de aislar la funcionalidad en componentes. Cuando hacemos esto si tenemos un elemento padre y dentro de ese padre esta otro elemento desde el padre no podemos hacer referencia a ese elemento. No podemos saber lo que hay dentro del padre. La informacion en react.js viaja como sabemos del padre a los hijos pero nunca de los hijos a al padre. Si queremos del hijo mandarle informacion al hijo no hay forma de hacerlo. A menos que dentro del componente hijo tengamos una funcion desde la cual el padre puede acceder a la informacion del hijo.

Vemos un ejemplo en el formulario. Vemos el componente ExmapleForwardRef.jsx y en App.jsx

El ref y forwardRef se usa mucho al aislar componentes, para airlar logica de negocio y cuando tengamos que integrar componentes de terceros.

*/
