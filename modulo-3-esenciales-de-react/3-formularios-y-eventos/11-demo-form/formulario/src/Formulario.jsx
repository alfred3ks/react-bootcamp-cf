import React from "react";
import style from "./Formulario.module.css";

// Creamos el componente formulario:
const Formulario = () => {
  // Vamos a crear una funcion para evitar el comportamiento del formulario de ser enviado:
  const onSubmit = (e) => {
    // Aqui vemos el evento sintetico de React.js
    console.log(e);

    e.preventDefault();
    console.log("Metodo onSubmit del formulario.");
  };

  // Funcion para segundo boton:
  const handleClick = (e) => {
    // Aqui tambien tenemos el evento sintetico de React.js:
    // e.stopPropagation();
    e.preventDefault();
    console.log("Hello guys!. Desde mi boton para stop propagation.");
  };

  const msj = () => {
    console.log("Mensaje ejecutado: Hello");
  };

  return (
    <div className={style.container}>
      <form
        action=""
        onSubmit={(e) => {
          // Paso las funciones que desee:
          onSubmit(e);
          msj();
        }}
      >
        <div className={style.label}>
          <label htmlFor="nombre">Nombre</label>
          <input type="text" id="nombre" />
        </div>

        <div className={style.label}>
          <label htmlFor="apellido">Apellidos</label>
          <input type="text" id="apellido" />
        </div>

        <button className={style.btn1}>Enviar</button>

        <button
          onClick={(e) => {
            handleClick(e);
          }}
          className={style.btn2}
        >
          Send
        </button>
      </form>
    </div>
  );
};

export default Formulario;
