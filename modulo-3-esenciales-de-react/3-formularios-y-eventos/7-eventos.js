/*
7 - Eventos:

Los eventos son interacciones que suceden sobre elemetos dentro del HTML. Cuando usamos JavaScript en paginas de HTML, JavaScript puede "reaccionar" sobre esos eventos.

Algunos de los eventos mas comunes que podemos encontrar:

- Click Events,
- Form Events,
- Keyboard Events,
- Mouse Events.

Repaso de como se veria eventos se forma nativa usando JavaScript:

const handleClick = function() {}

<button
  onclick= "handleClick">Click me
</button>

No todos los elementos tienes los mismos eventos disponibles, cada uno de ellos manejan diferentes eventos para hacer interacciones en la web.

Tenemos varios eventos mas comunes sobre un boton o en otros elementos:

- onclick,
- onchange,
- onload.

Para profundizar mas sobre los eventos sobre el DOM lo podemos ver en la web de w3schools:

https://www.w3schools.com/jsref/dom_obj_event.asp

Podemos ver los enventos que se ejecutan de manera nativa con JS.

No es necesario conocerlos todos para esto tenemos la documentacion, debemos saber es donde buscar y como implementarlos.

*/
