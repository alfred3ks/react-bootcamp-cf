/*

Los eventos se propagan del hijo al padre, vemos que al hacer click en el boton, este tiene el evento click y sus ansestros tambien entonces se ejecutan.

Para detener los eventos usamos el metodo de stopPropagation().

Cuando trabajamos con un formulario en los botones debemos usar el evento submit, este eveto su comportamiento natural es de enviar el formulario y de refrescar la misma, para este caso con JS solemos usar el evento preventDefault() para parar el comportamiento por defecto del evento submit.



*/
