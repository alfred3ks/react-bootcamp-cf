import { useEffect, useState } from "react";
import useTime from "./useTime";
import Clock from "./Clock";

const SelectorColor = () => {
  {
    /* Esto es un hook personalizado. */
  }
  const time = useTime();
  const [color, setColor] = useState("lightcoral");

  // Aqui tambien vemos un ejemplo de useEffect() con el array de dependencias, cuando cambiemos el color este se ejecutara:
  useEffect(() => {
    console.log(`El color seleccionado es: ${color}`);
  }, [color]);

  return (
    <div>
      <p>
        Pick a color:{" "}
        <select value={color} onChange={(e) => setColor(e.target.value)}>
          <option value="lightcoral">lightcoral</option>
          <option value="midnightblue">midnightblue</option>
          <option value="rebeccapurple">rebeccapurple</option>
        </select>
      </p>
      <Clock color={color} time={time.toLocaleTimeString()} />
    </div>
  );
};

export default SelectorColor;
