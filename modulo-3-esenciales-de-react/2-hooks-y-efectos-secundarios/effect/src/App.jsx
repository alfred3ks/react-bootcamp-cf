import { useEffect } from "react";
import SelectorColor from "./SelectorColor";
import useTime from "./useTime";

// Aqui tenemos nuestro componente App:
const App = () => {
  // Vemos useEffec() con dependencias:
  const time = useTime();

  useEffect(() => {
    console.log("Time se esta ejecutando");
  }, [time]);

  return (
    <div>
      <SelectorColor />
    </div>
  );
};

export default App;
