import { useState, useEffect } from "react";
// Este es un hook personalizado:
const useTime = () => {
  const [time, setTime] = useState(() => new Date());

  /* Aqui vemos el useEffect. */
  useEffect(() => {
    console.log("useEfect ejecutado.");
    const id = setInterval(() => {
      console.log("setinterval ejecutado.");
      setTime(new Date());
    }, 1000);

    // Esto es necesario cada vez que se usa un setInterval()
    return () => clearInterval(id);
  }, []);

  return time;
};

export default useTime;
