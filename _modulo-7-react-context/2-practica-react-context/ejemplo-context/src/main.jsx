import React from 'react'
import ReactDOM from 'react-dom/client'
import AppContext from './AppContext/AppContext'
import AppContextUno from './AppContextUno/AppContextUno'
import AppContextDos from './AppContextDos/AppContextDos'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    {/* <AppContext />*/}
    {/* <AppContextUno />*/}
    <AppContextDos />
  </React.StrictMode>
)
