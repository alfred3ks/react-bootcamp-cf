/*

Vercel: https://vercel.com/

En esta plataforma podemos hacer deploy de aplicaciones.
Podemos subir cualquier aplicacion pero ellos se especializan en cosas de frontend, soporta gran variedad  de framework, se puede utilizar el deploy a traves de git.

Utilizan cloudflare como CDN.

Haremos deploy del repo:

https://github.com/sergiodxa/cf-bootcamp-deploy

Lo vemos ya descargado en:
cf-bootcamp-deploy

Debemos loguearnos en vercel. Creamos una cuenta. Yo lo he autorizado con la cuenta de github.

Al loguearnos vemos que de el lado izquierdo salen una lista de todos los repos que tenemos en nuestro github. El que deseemos subir simplemente debemos darle al boton de import.

Se nos abrira una pantalla de configur project:

Podemos elegir el configurador del compilacion del proyecto, en este caso vite.
Con esto ya vemos que se configuran una comandos para hacer la build de la app.

Vemos el apartado Build and Output Settings:
BUILL COMMAND:
OUTPUT DIRECTORY:
INSTALL COMMAND:

Vemos que se activa el boton de hacer el DEPLOY.
Lo que hace es una revision por todo el panel de vercel.

Esto hay que verlo mas en profundidad porque es interesante para subir app a produccion.

*/