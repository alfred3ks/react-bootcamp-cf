/*

Diferencias entre una single page application y una multi page application.
Vemos unas imagenes en los assets llamada:

multi-page-application.png
simple-page-application.png

Una multi page application es la pagina web tradicional como siempre ha funcionado la web, el dispositivo le manda una peticion del tipo get a su servidor, este servidor procesa una respuesta, normalmente un documento html, el usuario recide este html, por ejemplo un formulario, ahi el usuario rellena los input de este formulario y envia este formulario por medio del metodo post al servidor, lo vemos en la imagen.

El servidor procesa la informacion, hace lo que debe, por ejemplo comprobar o actualizar una bd, comprobar credenciales, etc, y devuelve de nuevo al cliente un documento html.

Asi en lineas generales, el servidor recibe peticiones de cliente y devuelve respuestas en html.
Esto es un proceso algo lento, porque siempre devuelve todo un archivo html cuando solo debe cambiar un pequeño fragmento de la misma. No tenemos tampoco tanto control en cosas como transicion entre paginas, etc.

Debido a esto nacen las single page application. Vemos la imagen en los recursos.
Funciona muy similar a las multi page application, se hace una peticion get desde el cliente, el servidor recibe la informacion, la procesa, la preparar y la devuelve de nuevo al cliente como un archivo html.

Luego cuando el usuario, en el caso del formulario envia de nuevo los datos al servidor se hace mediante el metodo POST pero haciendo un fetch POST HTTP Request, el servidor recibe este POST, lo procesa de nuevo y en vez de mandar de nuesvo un html envia un documento .JSON, este documento JSON tiene la data que necesita la aplicacion del lado el cliente para actualizarse.

De esta forma hay mas control sobre todo el flujo de la aplicacion. La app ya no esta recibiendo cada vez archivos html. Sino que recibe pequeñas partes de informacion en formato JSON.

Seguimos...

*/