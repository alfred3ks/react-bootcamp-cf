/*

Ahora vamos a ver la diferencia entre server y serverless.
Tenemos en assets la imagen: server-serverless.png

server -> servidores que corren todo el dia sin importar si hay una request o no.

serverless -> nacio a partir de amazon web services, AWS, lanzaron este servicio landa una forma en que un usuario de una app solo se paga por cada peticion que reciba la app y no por estar corriendo continuamente. Solo se paga por el tiempo que corre la app y no por todo el dia.

Vercel usa su sistema de servidores como un serverless.

*/