/*

Veremos como va el despliegue de las app.
Profesor: Sergio Xalabri.
information for the 4 videos of the plataform:
Informacion para los 4 videos de la plataforma.

https://github.com/sergiodxa/cf-bootcamp-deploy

En este repositorio tenemos el codigo de la app que haremos deploy. lo haremos a vercel pero veremos los lineamientos generales.

Deployment.
Como funciona la web.

Hay un usuario que en su navegador escribe una url, cualquiera, ese navegador va a tener una cache, el cual es un lugar donde guarda recursos que ya accedio a ellos y si el recurso que el usuario esta tratandi de acceder ya esta en la cache la devuelve mostrando al usuario dicha pagina en un tiempo muy pero muy rapido.

Pero si la pagina no esta en la cache que podria ser lo mas normal, el navegador con la url suministrada va y hace una peticion a los DNI que es un servidor de dominios, Domain Name Service, el dns es simplemente una tabla que dice este dominio apunta a esta ip, y asi lo vemos con todos los dominios.

Existen un monton de DNS distribuidos por todo el mundo que estan sincronizados entre ellos, este DNS es el que le dice al navegador como comentamos ya anteriormente.
Ver imagen anexa como funciona la web.

como-funciona-la-web-png

El navegador tambien posee una cache de DNS para guardar y no estar pidiendo constantemente al servidor DNS las direcciones.

Una vez que el navegador posee la ip de esa url que le hemos pedido el navegador manda el request, normalmente este request al CDN, Content Delivery Network, otros servidores, distribuidos por todas partes del mundo, y estan todos tambien conectados entre si, son servidores localiados por regiones, si un usuario esta en europa no tiene que ir a un servidor en eeuu o canada, ya hay por europa repartidos. Todos esto no tardar tiempo en buscar las recursos.

Un proveedor de CDN es cloudflare, existen muchos.

Este CDN guarda una cache de las respuestas que envia el servidor, entonces este CDN cuando recibe una peticion de un cliente se fija si esta en la cache, para saber si ese request esta en la cache usar la url y las cabeceras (header) que se configuran para las peticiones, por ejemplo tenemos content type, el idioma del usuario.

Si esa cache guarda por ejemplo la html en español, cuando se pida en español devolvera a cliente una respuesta cacheada en español. Tambien si es en otro idioma.

Si en CDN no tiene la respuesta en cache, va al origin server, osea el servidor, el lugar donde hacemos el deploy de la app, en este origin server es un servidor http capas de recibir request interepretar lo que se pide y devolver esa peticion.
Este servidor en funcion de lo que le pidan puede ir a buscar un solo archivo, en disco, bien sea un html, o un archivo de una imagen, un archivo css, etc.

Tambien se puede dar el caso que nuestro proyecto sea una app realizada en nodeJS, osea una aplication server. Esta aplication server puede leer informacion de cualquier lado, por ejemplo un bd, o archivos en discon tambien. Dependera de como esta estructurada la app.

Cuando ya se recopila lo que se ha pedido el servidor genera una request, osea una respuesta.

Cuando el servidor emite una respuesta, el CDN verifica que esa respuesta tenga una cabecera de cache, si es asi la guarda para futuras peticiones.

El CDN lo envia el cliente y este muestra la informacion al usuario.

Asi en lineas generales funciona una peticion en la web.

*/