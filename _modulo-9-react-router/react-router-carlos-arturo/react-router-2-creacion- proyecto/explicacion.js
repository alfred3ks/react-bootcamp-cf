/*

Creamos un proyecto usando create-react app:

npx create-react-app app-blog

Suele tardar un poco la instalacion.

Hacemos la limpia normal de archivos. Ya tenemos la estructura basica de nuestro proyecto, ahora vamos a instalar react router:

Recuerda visitar la pagina web y ver la documentacion:

https://reactrouter.com/
https://reactrouter.com/docs/en/v6

npm install react-router-dom@6

OJO a veces cuando instalamos librerias o paquetes nos suele mostrar q en la instalacion hay ciertas vulnerabilidades y nos pone un comando para repararlas:

npm audit fix

Vamos a trabajar tambien con styled components asi que lo vamos a instalar:

npm install styled-components --save

Seguimos...

*/