import React from 'react';

const Blog = () => {
  return (
    <div>
      <h2>Blog</h2>
      <ul>
        <li>Articulo #1</li>
        <li>Articulo #2</li>
        <li>Articulo #3</li>
      </ul>
    </div>
  );
}

export default Blog;