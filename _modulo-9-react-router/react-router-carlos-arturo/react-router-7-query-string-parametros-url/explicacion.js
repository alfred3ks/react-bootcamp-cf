/*

QueryString y parametros URL.

En este apartado seguimos con el blog, al hacer click en un articulo debemos mostrar la pagina de ese articulo con su informacion, hasta ahora lo muestra en blanco.

Necesitamos crear una nueva ruta para los post. Lo vemos en el archivo App.js

Route path='/post/:id' element={<Post />}></Route>

Asi crearemos una ruta dinamica donde el valor de id sera el de cada articulo.

Tambien he creado un componente Post.js

Para poder mostrar el articulo que se le hace click debemos saber su id y accedemos a el por medio de un hook que nos captura el id de la barra del navegador.

El hook se llama useParams(). Es un hook de react router.

Ahora ya tenemos solucinado para que cuando se haga click a cada articulo nos muestre ese la single page de cada articulo.

Ahora si ese articulo no exite por ejemplo el usuario colocar en el navegador lo siguiente:

http://localhost:3000/post/aaaaa

La aplicacion se rompe. Lo vamos a solucionar con una simple comprobacion:

    <>
      {post
        ?
        <div>
          <h1>{post.titulo}</h1>
          <p>{post.texto}</p>
        </div>
        :
        <div>
          <h2>El artículo que buscas no existe...</h2>
        </div>
      }
    </>

Ahora como vemos si metemos cualquier valor que no sea un articulo de la data ese nos dira que no existe, ahi podemos devolver un componente, o podemos hacer una redireccion para eso tambien debemos usar el componente de react router Navigate.

Asi podemos hacer una redireccion a una pagina de error 404 de no encontrado.
    <>
      {post
        ?
        <div>
          <h1>{post.titulo}</h1>
          <p>{post.texto}</p>
        </div>
        :
        <Navigate replace to='/blog' />
      }
    </>


*/