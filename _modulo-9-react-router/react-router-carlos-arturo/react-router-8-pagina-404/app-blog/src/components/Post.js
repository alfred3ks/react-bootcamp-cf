import React from 'react';
import posts from '../data/posts';
import { useParams, Navigate } from 'react-router-dom';
import Error404 from './Error404';

const Post = () => {
  // Usamos un hook para obtener los parametros de la barra de navegador.
  // const parametros = useParams();
  const { id } = useParams();
  console.log(id);

  const post = posts[id - 1];
  return (
    <>
      {post
        ?
        <div>
          <h1>{post.titulo}</h1>
          <p>{post.texto}</p>
        </div>
        :
        <Error404 />
      }
    </>
  );
}

export default Post;