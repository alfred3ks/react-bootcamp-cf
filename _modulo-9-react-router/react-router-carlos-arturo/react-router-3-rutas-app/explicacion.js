/*

Ya tenemos instaladas nuestras dependencias ahora vamos a ver como crear las rutas de la aplicacion.

Vamos a trabajar en el archivo App.js:

importamos BrowserRouter:
import { BrowserRouter } from 'react-router-dom';

Y encerramos nuestra app dentro de este componente:

<BrowserRouter>
      <div>
        <header>
          <h1>Mi blog personal!!!</h1>
          <nav>
            <a href="#">Inicio</a>
            <a href="#">Blog</a>
            <a href="#">Acerca de</a>
          </nav>
        </header>
        <main>
          <div>
            <h2>Pagina de Inicio</h2>
            <p>Esta es ka página principal del sitio.</p>
          </div>
          <div>
            <h2>Blog</h2>
            <ul>
              <li>Articulo #1</li>
              <li>Articulo #2</li>
              <li>Articulo #3</li>
            </ul>
          </div>
          <div>
            <h2>Acerca de</h2>
            <p>Hola me llamo Alfredo...</p>
          </div>
        </main>
      </div>
</BrowserRouter>

Ahora vamos a ver un detalle de los enlaces, como lo trabajremos en React:

<nav>
  <a href="/">Inicio</a>
  <a href="/blog">Blog</a>
  <a href="/acerca-de">Acerca de</a>
</nav>

Si lo hacemos de esta manera cada vez que se haga click en cada enlace la app se actualizara y no queremos eso, para eso tenemos un componente llamado NavLink:
Lo importamos en la cabecera:

import { BrowserRouter, NavLink } from 'react-router-dom';

<nav>
  <NavLink to="/">Inicio</NavLink>
  <NavLink to="/blog">Blog</NavLink>
  <NavLink to="/acerca-de">Acerca de</NavLink>
</nav>

Ahora vemos que la web no se refresca pero si va a esos enlaces. Eso solucionado ahora vamos con las rutas: Para esto debemos importar un componente llamado Router en la cabecera:

import { BrowserRouter, NavLink, Route } from 'react-router-dom';

<Route  element={
  <div>
    <h2>Pagina de Inicio</h2>
    <p>Esta es ka página principal del sitio.</p>
  </div>
}/>

Asi con el resto de elementos.

Ahora debemos importar otro nuevo componente: Routes, esto es un contenedore de las rutas:

<BrowserRouter>
  <div>
    <header>
      <h1>Mi blog personal!!!</h1>
      <nav>
        <NavLink to="/">Inicio</NavLink>
        <NavLink to="/blog">Blog</NavLink>
        <NavLink to="/acerca-de">Acerca de</NavLink>
      </nav>
    </header>
    <main
      <Routes>
        <Route path='/' element={
          <div>
            <h2>Pagina de Inicio</h2>
            <p>Esta es ka página principal del sitio.</p>
          </div>
        } />
        <Route path='/blog' element={
          <div>
            <h2>Blog</h2>
            <ul>
              <li>Articulo #1</li>
              <li>Articulo #2</li>
              <li>Articulo #3</li>
            </ul>
          </div>
        } />
        <Route path='/acerca-de' element={
          <div>
            <h2>Acerca de</h2>
            <p>Hola me llamo Alfredo...</p>
          </div>
        } />
      </Routes>
    </main>
  </div>
</BrowserRouter>

Aqui tenemos varios detalles, envolvemos el componete dentro de BroserRouter. Es como un contenedor y para las rutas vemos el componente Routes con el path y el atributo element={}

Ultimo video visto: Rutas...

Seguimos...

*/