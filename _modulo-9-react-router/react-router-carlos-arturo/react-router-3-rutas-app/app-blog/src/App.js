import React from 'react';
import { BrowserRouter, NavLink, Route, Routes } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div>
        <header>
          <h1>Mi blog personal!!!</h1>
          <nav>
            {/* Esto es lo recomendado en vez de los ancla */}
            <NavLink to="/">Inicio</NavLink>
            <NavLink to="/blog">Blog</NavLink>
            <NavLink to="/acerca-de">Acerca de</NavLink>
          </nav>
        </header>
        <main>

          <Routes>
            <Route path='/' element={
              <div>
                <h2>Pagina de Inicio</h2>
                <p>Esta es ka página principal del sitio.</p>
              </div>
            } />
            <Route path='/blog' element={
              <div>
                <h2>Blog</h2>
                <ul>
                  <li>Articulo #1</li>
                  <li>Articulo #2</li>
                  <li>Articulo #3</li>
                </ul>
              </div>
            } />
            <Route path='/acerca-de' element={
              <div>
                <h2>Acerca de</h2>
                <p>Hola me llamo Alfredo...</p>
              </div>
            } />
          </Routes>
        </main>
      </div>
    </BrowserRouter>
  );
}

export default App;
