/*

Para crear nuestra navegacion interna vamos a ver un componente llamado NavLink, es un componente que nos provee react router para crear la navegacion interna de la app y evitar que cada ves que cambiemos de pagina se haga la patecion al servidor de manera nativa, de esta manera solo se renderizara el componente y no se recargara la pagina.

Creamos nuestro componente para crear la ruta de navegacion llamado NavBar.jsx que nos quedaria asi:

import React from 'react'

const NavBar = () => {
  return (
    <nav>
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/about">About</a></li>
        <li><a href="/users">Users</a></li>
      </ul>
    </nav>
  )
}

export default NavBar;

Ahora como vemos si lo creamos asi al navegar por cada uno de los apartados la web se recarga entera, eso no es lo que queremos, lo que queremos es que se recargue solo el componente que es en realidad lo unico que cambia en la app. Para eso entra en juego el componente NavLink que nos provee react router.

Lo importamos en la cabecera y sustituimos la etiqueta a del ancla por el componente NavLink. Nos quedaria asi:

import React from 'react';
import { NavLink } from 'react-router-dom';

const NavBar = () => {
  return (
    <nav>
      <ul>
        <li><NavLink to='/'>Home</NavLink></li>
        <li><NavLink to='/About'>About</NavLink></li>
        <li><NavLink to='/Users'>Users</NavLink></li>
      </ul>
    </nav>
  )
}

export default NavBar;

Ahora vamos a ver un tema interesante de este componente NavLink, por defecto este componente tiene una clase css active. Esto lo podemos ver en el navegador en los opciones de desarrollador:

<a class="active" href="/" aria-current="page">Home</a>

Nosotros podemos usar esa clase para mostrar al usuario cuando hace click sobre ese elemento del nav que es el activo.

Creamos los estilos css:

css/style.css

Nos quedaria asi la implementacion de esos estilos:

import React from 'react';
import { NavLink } from 'react-router-dom';
import '../css/style.css'

const NavBar = () => {
  return (
    <nav>
      <ul>
        <li><NavLink to='/' className={({ isActive }) => isActive ? 'active' : 'noActive'}>Home</NavLink></li>
        <li><NavLink to='/About' className={({ isActive }) => isActive ? 'active' : 'noActive'}>About</NavLink></li>
        <li><NavLink to='/Users' className={({ isActive }) => isActive ? 'active' : 'noActive'}>Users</NavLink></li>
      </ul>
    </nav>
  )
}

export default NavBar;

className={({ isActive }) => isActive ? 'active' : 'noActive'}

Como vemos usamos uno u otro en funcion de la clase active.

Para mas detalle lo podemos ver en la documentacion:

https://reactrouter.com/docs/en/v6/components/nav-link





*/