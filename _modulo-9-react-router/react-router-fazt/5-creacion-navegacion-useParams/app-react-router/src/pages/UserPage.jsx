import React from 'react';
import { useParams } from 'react-router-dom';

const UserPage = () => {

  //Asi capturamos el valor de la ruta creada /users/ que vemos en la ruta en App.js
  // const params = useParams();
  // console.log(params);
  const { id } = useParams();
  console.log(id);

  return (
    <div>
      <h1>User: {id}</h1>
    </div>
  )
}

export default UserPage;