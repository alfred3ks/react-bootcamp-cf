/*

React router nos proporciona un hook para poder ver los parametros de la url.
Para verlo vamos a crear una pagina nueva en pages.

Creamos la ruta que queremos hacer la consulta en App.js:

<Route path='/users/:id' element={<UserPage />} />

Luego creamos nuestra pagina que vamos a mostrar, osea el componente que se renderizara al hacer esa busqueda por la url:

pages/UserPage.jsx

Nos quedaria asi:

import React from 'react';
import { useParams } from 'react-router-dom';

const UserPage = () => {

  //Asi capturamos el valor de la ruta creada /users/ que vemos en la ruta en App.js
  // const params = useParams();
  // console.log(params);
  const { id } = useParams();
  console.log(id);

  return (
    <div>
      <h1>User: {id}</h1>
    </div>
  )
}

export default UserPage;

Con el hooks useParams podemos obtener el objeto que le pasemos a la url. Bastante interesante...

Seguimos...

*/