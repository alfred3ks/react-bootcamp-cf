import React from 'react';
import { useNavigate, Link, Outlet } from 'react-router-dom';

const Dashboard = () => {

  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/');
  }

  return (
    <div>
      <h1>Bienvenido a tu Dashboard</h1>
      <button onClick={handleClick}>Logout</button>
      <br />
      <Link to='welcome'>Say Welcome!!!!</Link>
      <br />
      <Link to='goodbye'>Say GoodBye!!!</Link>
      <Outlet />
    </div>
  )
}

export default Dashboard;