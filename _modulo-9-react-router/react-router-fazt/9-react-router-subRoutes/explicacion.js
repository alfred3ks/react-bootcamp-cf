/*

Cuando tengamos que usar subrutas lo mas recomendado es hacerlo directamente en App.jsx
Para la organizacion del codigo es lo mejor poner todas las rutas y subrutas juntas.

Nuestro ruta dashboard queremos que muestre un componente al usar la ruta dashboard/welcome, en nuestro componente App.jsx quedaria asi:

<Route path='/dashboard/*' element={<Dashboard />} >
  <Route path='welcome' element={<Welcome />} />
</Route>

Recordar poner el * para decirle que tiene subrutas dentro, se la pasamos dentro.

Lugo donde deseemos renderizar esa subruta debemos usar el componente Outlet, importarlo de react router:

import React from 'react';
import { useNavigate, Link, Outlet } from 'react-router-dom';

const Dashboard = () => {

  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/');
  }

  return (
    <div>
      <h1>Bienvenido a tu Dashboard</h1>
      <button onClick={handleClick}>Logout</button>
      <Link to='welcome'>Ir a welcome</Link>
      <Outlet />
    </div>
  )
}

export default Dashboard;

Lo podemos colocar donde queramos dentro del return.

Ahora vamos a colocar otra subruta y veamos como se muestra:
App.jsx:

<Route path='/dashboard/*' element={<Dashboard />} >
  <Route path='welcome' element={<Welcome />} />
  <Route path='goodbye' element={<GoodBye />} />
</Route>

import React from 'react';
import { useNavigate, Link, Outlet } from 'react-router-dom';

const Dashboard = () => {

  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/');
  }

  return (
    <div>
      <h1>Bienvenido a tu Dashboard</h1>
      <button onClick={handleClick}>Logout</button>
      <br />
      <Link to='welcome'>Say Welcome!!!!</Link>
      <br />
      <Link to='goodbye'>Say GoodBye!!!</Link>
      <Outlet />
    </div>
  )
}

export default Dashboard;

Independiente del click al enlace se renderizara lo que pulsemos bien sea welcome o goodbye. Una anidacion de rutas.

Muy interesante!!!!

Final... Ahora a practicar...

https://www.youtube.com/watch?v=7xRVnmWcTE8&t=2169s

*/