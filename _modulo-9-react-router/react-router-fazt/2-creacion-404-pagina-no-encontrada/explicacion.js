/*

Ya hemos creado las rutas, como podemos ver al colocar en el navegador las rutas que hemos creado estas se muestran, pero cuando ponemos una ruta que no tenemos configurada deberia mostrar una pagina de error 404 de paginano encontrada. Procederemos a crearla.

Para eso React Router nos ofrece el path * que quiere decir para el resto de rutas muestra esto. Nos quedaria asi:

<Route path='*' element={<Error404 />} />

Ahora veremos como crear una barra de navegacion para que un usuario pulse sobre eso enlaces y pueda acceder a las diferentes enlaces. Una cosa que vemos que al colocar en los enlaces del navegador las diferentes direcciones la pagina se refresca y eso no lo que queremos ya que lo unico que debe cambiar es el componente que queremos renderizar.

*/