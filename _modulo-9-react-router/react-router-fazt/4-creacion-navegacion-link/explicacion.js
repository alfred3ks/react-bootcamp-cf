/*

Ahora vamos a ver el componente link de React Router:

Su funcion es muy parecido a NavLink, funciona como un elemento <a href=""></a> y nos permite hacer el redireccionamiento de diferentes partes de la app.

Vamos a trabajar en la pagina de Home.jsx y crearemos unos enlaces.
La diferencia con NavLink es que esta tiene una clase active por defecto y Link no.

Nos quedaria asi:

import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  const userId = 3;
  return (
    <div>
      <h1>Application</h1>
      <div>
        <Link to="/users">Ir a Users</Link>
      </div>
      <div>
        <Link to={`/users/${userId}`}>Ir a la pagina del usuario: {userId}</Link>
      </div>
    </div>
  )
}

export default Home;

*/