import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  const userId = 'Alfred';
  return (
    <div>
      <h1>Application</h1>
      <div>
        <Link to="/users">Ir a Users</Link>
      </div>
      <div>
        <Link to={`/users/${userId}`}>Ir a la pagina del usuario: {userId}</Link>
      </div>
    </div>
  )
}

export default Home;