/*

Vamos a conocer el componente Navigate.

Este componente lo solemos usar para hacer las redirecciones. Lo debemos importar desde react router.

Lo importamos en App.jsx
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';

<Route path='/contact' element={<Navigate replace to='/users' />} />

Cuando en el navegador buscamos:
http://127.0.0.1:5173/contact

Nos redirecciona a:

http://127.0.0.1:5173/users

Esto es interesante cuando un usuario cierra sesion se redirecciona al home de la web a la pagina que queramos.

El atributo replace si lo ponemos cuando hace la redireccion ya no permite volver atras con los botones del navegador.

*/