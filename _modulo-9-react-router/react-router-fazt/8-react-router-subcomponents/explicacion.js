/*

Ahora vamos a ver como colocar un componente dentro de otro. Veremos como crear un subcomponente.

Vamos hacer que dentro d ela pagina dashboard al cargarla se nos muestre un componente.

Osea es hacer que una ruta este dentro de otra.

http://127.0.0.1:5173/dashboard/welcome

En App.jsx la ruta:

<Route path='/dashboard/*' element={<Dashboard />} />

Usamos el * para decirle que dentro de dashboart van a existir otras rutas.

Nuestro componente dashboard.jsx nos quedaria asi con la ruta:

import React from 'react';
import { useNavigate, Route, Routes, Link } from 'react-router-dom';
import Welcome from '../components/Welcome';

const Dashboard = () => {

  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/');
  }

  return (
    <div>
      <h1>Bienvenido a tu Dashboard</h1>
      <Routes>
        <Route path='welcome' element={<Welcome />} />
      </Routes>
      <button onClick={handleClick}>Logout</button>
      <Link to='welcome'>Ir a welcome</Link>
    </div>
  )
}

export default Dashboard;

Para verlo tendriamos que colocar la ruta:
http://127.0.0.1:5173/dashboard/welcome

O dandolo click al boton de ir a welcome.

*/