import React from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Home from './pages/Home';
import About from './pages/About';
import Users from './pages/Users';
import Error404 from './pages/Error404';
import NavBar from './components/NavBar';
import UserPage from './pages/UserPage';
import Dashboard from './pages/Dashboard';

const App = () => {

  return (

    <div>
      <h1>React Router</h1>
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/about' element={<About />} />
          <Route path='/users' element={<Users />} />
          <Route path='/dashboard/*' element={<Dashboard />} />
          <Route path='/contact' element={<Navigate replace to='/users' />} />
          <Route path='/users/:id' element={<UserPage />} />
          <Route path='*' element={<Error404 />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App;
