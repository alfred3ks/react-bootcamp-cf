import React from 'react';
import { useNavigate, Route, Routes, Link } from 'react-router-dom';
import Welcome from '../components/Welcome';

const Dashboard = () => {

  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/');
  }

  return (
    <div>
      <h1>Bienvenido a tu Dashboard</h1>
      <Routes>
        <Route path='welcome' element={<Welcome />} />
      </Routes>
      <button onClick={handleClick}>Logout</button>
      <Link to='welcome'>Ir a welcome</Link>
    </div>
  )
}

export default Dashboard;