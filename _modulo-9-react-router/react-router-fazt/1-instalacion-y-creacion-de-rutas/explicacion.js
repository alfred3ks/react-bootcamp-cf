/*

Vamos a empezar a trabajar con React Router.
React router es una libreria que nos permite crear enrutadores en nuestra app. Los enrutadores son paquetes que nos permiten crear url. Es decir dividir una app en diferentes secciones.

Por ejemplo tendriamos una app de la siguiente manera:

app/home
app/users
app/about

Al final esto es una simple page application ya que lo que hacemos es renderizar en la misma pagina las diferentes rutas de la app.

La version que usaremos es la version 6. Es la ultima que podemos encontrar para react router.

Vamos a crear nuestra app usando vite. Haremos una limpia de la app para dejarla lista para funcionar.

  npm create vite@latest app-react-router -- --template react

O podemos hacerlo asi y seguir los pasos:

  npm create vite@latest app-react-router

Hacemos limpia de los archivos, ahora ya podemos instalar react router.

  npm install react-router-dom

Como nuestra app es una app web usaremos react router dom. Usaremos la del dom porque lo que haremos una app del navegaodor porque tambien puede usarse para react native.

Ahora una vez instalado la libreria debemos saber que para usarlo debemos encerrar nuestra app dentro del componente BrowserRouter que lo importaremos de react-router-dom, ademas existe diferentes formas de usar este componente, hay quien lo usa para envolver el componente <App/> dentro del main.jsx y otros que lo usan dentro del propio componente App.js, ese sera el caso nuestro lo usaremos ahi.

Todo lo que pongamos dentro del componente BrowserRouter este interpretara las rutas. Alli es donde crearemos las diferentes rutas de nuestra app.

Tambien para crear las rutas necesitaremos importar los componentes Router y Route:

import { BrowserRouter, Route, Routes } from 'react-router-dom';

El componente Route: Recibe dos parametros uno es el path que seria la ruta que deseamos renderizar y luego el elemento que vamos a renderizar.

<Route path='' element={}/>

<Route path='/' element={<div><h1>Hola mundo!!!</h1></div>} />

<BrowserRouter>
  <Routes>
    <Route path='/' element={<div><h1>Hola mundo!!!</h1></div>} />
  </Routes>
</BrowserRouter>

Tambien podemos crear un componente y pasarlo en element.
Lo hacemos.

components/Home.jsx
components/About.jsx

<BrowserRouter>
  <Routes>
    <Route path='/' element={<Home />} />
    <Route path='/about' element={<About />} />
  </Routes>
</BrowserRouter>

Para mostrar las paginas que queremos usaremos una carpeta llamada pages donde vamos a crear nuestros componentes que integraran la paginas que vamos a mostrar.

Creamos componentes:
pages/About.jsx
pages/Home.jsx

<BrowserRouter>
  <Routes>
    <Route path='/' element={<Home />} />
    <Route path='/about' element={<About />} />
    <Route path='/users' element={<Users />} />
  </Routes>
</BrowserRouter>

Como vemos esta es la manera de crear rutas para nuestra app.


*/