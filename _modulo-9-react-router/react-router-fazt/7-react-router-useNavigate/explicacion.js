/*

Ya vimos el componente Navigate que usaremos para hacer las redicrecciones.

Tambien podemos usar esta funcionalidad a traves de un hook. useNavigate(), tambien lo proporciona React Router.

Vamos a ir a pages y vamos a crear una pagina nueva:
Dashboard.jsx

Vamos a App.jsx y creamos esa ruta:

<Route path='/dashboard' element={<Dashboard />} />

http://127.0.0.1:5173/dashboard

Bien hasta aqui bien.

Ahora supongamos que desde ese dashboard queremos cerrar sesion de la app.

Para esto vemos como el hook de navigate podemos redireccionar a cualquier enlace de la pagina en este caso al hacer click al boton de cerrar sesion. OJO esto es solo una funcion pero se puede usar para hacer cualquier redireccion, imaginacion al poder.

import React from 'react';
import { useNavigate } from 'react-router-dom';

const Dashboard = () => {

  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/');
  }

  return (
    <div>
      <h1>Bienvenido a tu Dashboard</h1>
      <button onClick={handleClick}>Logout</button>
    </div>
  )
}

export default Dashboard;


*/