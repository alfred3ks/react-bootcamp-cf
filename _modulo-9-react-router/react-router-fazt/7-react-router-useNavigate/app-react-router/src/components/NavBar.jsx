import React from 'react';
import { NavLink } from 'react-router-dom';
import '../css/style.css'

const NavBar = () => {
  return (
    <nav>
      <ul>
        <li><NavLink to='/' className={({ isActive }) => isActive ? 'active' : 'noActive'}>Home</NavLink></li>
        <li><NavLink to='/About' className={({ isActive }) => isActive ? 'active' : 'noActive'}>About</NavLink></li>
        <li><NavLink to='/Users' className={({ isActive }) => isActive ? 'active' : 'noActive'}>Users</NavLink></li>
      </ul>
    </nav>
  )
}

export default NavBar;