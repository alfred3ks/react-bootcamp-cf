// import BasicForm from './components/1BasicForm';
// import BasicFormSinFormik from './components/2BasicFormSinFormik';
// import BasicFormConFormik from './components/2BasicFormConFormik';
// import BasicFormValidation from './components/3BasicFormValidation';
import BasicFormComponent from './components/4BasicFormComponent';

function App() {
  return (
    <div>
      <BasicFormComponent />
    </div>
  )
}

export default App;
