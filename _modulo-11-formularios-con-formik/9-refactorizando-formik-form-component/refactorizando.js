/*

Vamos a refactorizar el codigo creado. Vemos que existe mucho codigo que se repite.
Veremos el componente 3BasicFormValidation.jsx

Vamos a ver el componente Formik component:

Funciones y componentes de Formik
● getFieldsProps function
  ○ Reducir props repetido como name, value, onChange, onBlur
● Formik component
  ○ Encierra todo el formulario en un contexto en lugar de usar useFormik
● Form component
  ○ Devuelve una etiqueta form con todos los props de Formik (handleSubmit, handleReset)
● Field component
  ○ Devuelve una etiqueta input con todos los props de la función getFieldProps
● ErrorMessage component
  ○ Agrega la condición cuando input es usado (touched) y si existe el error para mostrar un mensaje

Ahora nos toca ver Form component: Voy a crear otro componente para variar los datos ahi:
4BasicFormComponent.jsx

Debemos importar este componente desde formik:
import { Formik, Form } from 'formik';

Vamos a sustituir la etiqueta de form por Form.

Ya nos podemos quitar el atributo que recibe el form: Ya no es necesario:
onSubmit={formik.handleSubmit}

Tambien ya no es neceasirio el onClick de boton:
onClick={formik.resetForm}



*/