/*

Vamos a refactorizar el codigo creado. Vemos que existe mucho codigo que se repite.
Veremos el componente 3BasicFormValidation.jsx

Vamos a ver el componente Formik component:

Funciones y componentes de Formik
● getFieldsProps function
  ○ Reducir props repetido como name, value, onChange, onBlur
● Formik component
  ○ Encierra todo el formulario en un contexto en lugar de usar useFormik
● Form component
  ○ Devuelve una etiqueta form con todos los props de Formik (handleSubmit, handleReset)
● Field component
  ○ Devuelve una etiqueta input con todos los props de la función getFieldProps
● ErrorMessage component
  ○ Agrega la condición cuando input es usado (touched) y si existe el error para mostrar un mensaje

Este componente Formik component encierra el componente en un contexto algo parecido a lo que hemos visto useContext.

Lo usaremos en el form y veremos como todo ese form puede compartir los estados.

importamos ese componente:
import { useFormik, Formik } from 'formik';

Sacamos dentro del componente las funciones de validacion:

  // Aqui creamos la funcion validate para las validaciones: Esta funcion debe retornar un objeto
  function validate(values) {
    let errors = {};
    // Validacion del email:
    if (values.email.length === 0) {
      errors.email = 'Email Required';
    } else {
      if (!/[a-z0-9]+@[a-z]+\.[a-z]{2,3}/i.test(values.email)) {
        errors.email = "Invalid format";
      }
    }

    // Validacion del firstName:
    if (values.firstName.length === 0) {
      errors.firstName = 'FirstName required'
    } else if (values.firstName.length <= 5) {
      errors.firstName = 'Should be more than 5 characters'
    }
    // Validacion del lastName:
    if (values.lastName.length === 0) {
      errors.lastName = 'lastName required'
    } else if (values.lastName.length <= 5) {
      errors.lastName = 'Should be more than 5 characters'
    }
    return errors;
  }

  function handleSubmit(values) {
    console.log(values);
    console.log(values.email, values.firstName, values.lastName);
  }

Y encerramos nuestro form dentro del componente: Le pasamos por props cada uno de los valores iniciales.

<Formik initialValues={}>

</Formik>

<Formik
        initialValues={{ email: '', firstName: '', lastName: '', }}
        onSubmit={handleSubmit}
        validate={validate}
      >

Ya podemos tambien prescindir del objeto formik:

  const formik = useFormik({
    initialValues: {
      email: '',
      firstName: '',
      lastName: '',
    },
    onSubmit: handleSubmit,
    validate,
  }
  )

Vemos que se rompe la app: Pero debemos crear una funcion que le pasara por parametro formik al formulario:

{(formik)=>(
  Aqui pasamos el formulario. form.
)}

Seguimos...

*/