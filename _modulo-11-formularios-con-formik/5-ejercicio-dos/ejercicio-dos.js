/*

Vamos a ver un segundo ejercicio.

Vamos a convertir el formulario anterior en un formulario mas dinamico.
- Haremos que los imputs de firstName y lastName aparezcan despues de que el input email tenga un valor.

Vemos un componente que haremos esto sin usar formik, 2BasicFormSinFormik.jsx.

Lo que hariamos es crear estados para cada uno de los input. Lo vemos en el componente:
2BasicFormSinFormik.jsx

Ahora vemos el componente con formik:
2BasicFormConFormik.jsx



*/