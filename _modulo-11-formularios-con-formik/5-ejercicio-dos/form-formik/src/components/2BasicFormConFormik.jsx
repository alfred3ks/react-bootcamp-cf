import { useFormik } from 'formik';

const BasicFormConFormik = () => {

  const formik = useFormik({
    initialValues: {
      email: '',
      firstName: '',
      lastName: '',
    },
    onSubmit: handleSubmit
  }
  )

  function handleSubmit(values) {
    console.log(values);
    console.log(values.email, values.firstName, values.lastName);
  }

  return (
    <div>
      <h1>Basic Form</h1>
      <form onSubmit={formik.handleSubmit}>
        <div>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange} />
        </div>

        {/* Aqui hacemos la validacion para mostrar los input. */}

        {formik.values.email.length > 0
          ?
          (
            <>
              <div>
                <label htmlFor="firstName">First Name</label>
                <input
                  id="firstName"
                  type="text"
                  name="firstName"
                  value={formik.values.firstName}
                  onChange={formik.handleChange}
                />
              </div>

              <div>
                <label htmlFor="lastName">Last Name</label>
                <input
                  id="lastName"
                  type="text"
                  name="lastName"
                  value={formik.values.lastName}
                  onChange={formik.handleChange}
                />
              </div>
            </>
          )
          :
          null}
        <button type="submit">Subscribe</button>
        <button type="reset" onClick={formik.resetForm}>Reset</button>
      </form>
    </div>
  );
}

export default BasicFormConFormik;