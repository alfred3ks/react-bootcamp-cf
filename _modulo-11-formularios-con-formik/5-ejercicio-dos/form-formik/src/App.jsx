// import BasicForm from './components/1BasicForm';
// import BasicFormSinFormik from './components/2BasicFormSinFormik';
import BasicFormConFormik from './components/2BasicFormConFormik';

function App() {
  return (
    <div className="App">
      <BasicFormConFormik />
    </div>
  )
}

export default App;
