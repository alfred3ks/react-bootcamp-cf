/*

Vamos a ver unas funciones adicionales que tiene Formik.
● setFieldValue function
  ○ Permite actualizar los values del estado del formulario
● FieldArray component
  ○ Ayuda a manejar arrays dentro de los values del form
● useFormikContext component
  ○ Custom hook que te permite acceder a los valor del contexto del componente Formik
Functiones y componentes extra

● FieldArray component
  ○ Ayuda a manejar arrays dentro de los values del form

Tenenos un componente llamado FieldArray, nos sirve para poder manejar arrays dentro de value del formulario.

Esto se ajusta al ejercicio que tenemos.
Vemos el componente:

8formikFieldArrayComponent.jsx

Vamos a importar:
import { Formik, Form, Field, ErrorMessage, FieldArray } from 'formik';

<FieldArray name="invitedFriends">
  Encerramos en una funcion:
{
  ({ remove, push }) => (
    <>
      {formik.values.invitedFriends.map((_friend, index) => {
        return (
          <div key={index}>
            <Field
              type="text"
              name={`invitedFriends[${index}]`}
            />
            <ErrorMessage name={`invitedFriends[${index}]`} />
            <button
              type="button"
              onClick={() => remove(index)}
            >
              X
            </button>
          </div>
        );
      })}
      <button
        type="button"
        onClick={() => push("")}>
        Add Friend
      </button>
    </>
  )
}
</FieldArray >

Encerramos dentro del componente <FieldArray></FieldArray> esa parte del codigo.

Vamos a refactorizar el codigo en componentes especificos:



*/