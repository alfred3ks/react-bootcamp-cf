// import BasicForm from './components/1BasicForm';
// import BasicFormSinFormik from './components/2BasicFormSinFormik';
// import BasicFormConFormik from './components/2BasicFormConFormik';
// import BasicFormValidation from './components/3BasicFormValidation';
// import BasicFormComponent from './components/4BasicFormComponent';
// import BasicFormFieldComponent from './components/5BasicFormFieldComponent';
// import BasicFormYup from './components/6BasicFormYup';
// import FormikSetValueFunction from './components/7FormikSetValueFunction';
import FormikFieldArrayComponent from './components/8FormikFieldArrayComponent';

function App() {
  return (
    <div>
      <FormikFieldArrayComponent />
    </div>
  )
}

export default App;
