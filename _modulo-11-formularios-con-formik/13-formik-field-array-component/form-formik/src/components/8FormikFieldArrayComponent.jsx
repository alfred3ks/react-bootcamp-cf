import {
  Formik,
  Form,
  Field,
  ErrorMessage,
  FieldArray,
  useFormikContext,
} from "formik";

// Importamos Yup:
import * as Yup from "yup";

function handleSubmit(values) {
  console.log(values);
  console.log(values.email, values.firstName, values.lastName);
}

// Creamos el schema de validacion:
const newlettersSchema = Yup.object({
  email: Yup.string().email("Invalid format").required("Required"),
  firstName: Yup.string()
    .required("Required")
    .min(5, "should have at least 5 caracters"),
  lastName: Yup.string()
    .required("Required")
    .min(5, "should have at least 5 caracters"),
  invitedFriends: Yup.array(Yup.string().email()),
});

// Refactorizamos:
const InvitedFriendsSections = () => {
  // Usamos useFormikContext: otro contexto:
  const { values } = useFormikContext();

  return (
    <div>
      <div>Invited Friends:</div>
      {/* Implementamos el componente FieldArray: */}
      <FieldArray name="invitedFriends">
        {/* Encerramos en una funcion */}
        {({ remove, push }) => (
          <>
            {values.invitedFriends.map((_friend, index) => {
              return (
                <div key={index}>
                  <Field type="text" name={`invitedFriends[${index}]`} />
                  <ErrorMessage name={`invitedFriends[${index}]`} />
                  <button type="button" onClick={() => remove(index)}>
                    X
                  </button>
                </div>
              );
            })}
            <button type="button" onClick={() => push("")}>
              Add Friend
            </button>
          </>
        )}
      </FieldArray>
    </div>
  );
};

/* Mas refacturacion del codigo otro componente */
const AdditionalInfoSection = () => {
  const { values } = useFormikContext();
  if (values.email.length === 0) return null;

  return (
    <>
      <div>
        <label htmlFor="firstName">First Name</label>
        <Field id="firstName" type="text" name="firstName" />
        <ErrorMessage name="firstName" />
      </div>
      <div>
        <label htmlFor="lastName">Last Name</label>
        <Field id="lastName" type="text" name="lastName" />
        <ErrorMessage name="lastName" />
      </div>

      <InvitedFriendsSections />
    </>
  );
};

/* Aqui tenemos el componente principal */
const FormikFieldArrayComponent = () => {
  return (
    <div>
      <h1>Basic Form</h1>
      {/* Al componente Formik pasamos los atributos del objeto formik que creamos con useFormik */}
      <Formik
        initialValues={{
          email: "",
          firstName: "",
          lastName: "",
          invitedFriends: [],
        }}
        onSubmit={handleSubmit}
        validationSchema={newlettersSchema}
      >
        <Form>
          <div>
            <label htmlFor="email">Email</label>
            <Field id="email" type="email" name="email" />
            <ErrorMessage name="email" />
          </div>

          <AdditionalInfoSection />

          <button type="submit">Subscribe</button>
          <button type="reset">Reset</button>
        </Form>
      </Formik>
    </div>
  );
};

export default FormikFieldArrayComponent;
