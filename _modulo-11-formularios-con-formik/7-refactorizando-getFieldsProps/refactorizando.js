/*

Vamos a refactorizar el codigo creado. Vemos que existe mucho codigo que se repite.
Veremos el componente 3BasicFormValidation.jsx

Vamos a usar unos metodos de formik que nos ayudan a eso:

Funciones y componentes de Formik
● getFieldsProps function
  ○ Reducir props repetido como name, value, onChange, onBlur
● Formik component
  ○ Encierra todo el formulario en un contexto en lugar de usar useFormik
● Form component
  ○ Devuelve una etiqueta form con todos los props de Formik (handleSubmit, handleReset)
● Field component
  ○ Devuelve una etiqueta input con todos los props de la función getFieldProps
● ErrorMessage component
  ○ Agrega la condición cuando input es usado (touched) y si existe el error para mostrar un mensaje

Veamos la validacion en input email:
{...formik.getFieldProps('email')}

Con el spread operator le decimos que herede todo lo de formik osea el objeto que hemos creado arriba que coincide que se llama asi, error a mi manera de ver porque crea confusion, pero bueno... y le pasamos el primer metodo pasandole como props el valor de name del input.

Lo hacemos con el resto de input.

{...formik.getFieldProps('firstName')}
{...formik.getFieldProps('lastName')}


*/