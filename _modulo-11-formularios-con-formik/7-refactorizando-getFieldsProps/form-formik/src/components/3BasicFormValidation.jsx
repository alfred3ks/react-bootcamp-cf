import { useFormik } from 'formik';

const BasicFormValidation = () => {

  const formik = useFormik({
    initialValues: {
      email: '',
      firstName: '',
      lastName: '',
    },
    onSubmit: handleSubmit,
    validate,
  }
  )

  // Aqui creamos la funcion validate para las validaciones: Esta funcion debe retornar un objeto
  function validate(values) {
    let errors = {};
    // Validacion del email:
    if (values.email.length === 0) {
      errors.email = 'Email Required';
    } else {
      if (!/[a-z0-9]+@[a-z]+\.[a-z]{2,3}/i.test(values.email)) {
        errors.email = "Invalid format";
      }
    }

    // Validacion del firstName:
    if (values.firstName.length === 0) {
      errors.firstName = 'FirstName required'
    } else if (values.firstName.length <= 5) {
      errors.firstName = 'Should be more than 5 characters'
    }
    // Validacion del lastName:
    if (values.lastName.length === 0) {
      errors.lastName = 'lastName required'
    } else if (values.lastName.length <= 5) {
      errors.lastName = 'Should be more than 5 characters'
    }
    return errors;
  }

  function handleSubmit(values) {
    console.log(values);
    console.log(values.email, values.firstName, values.lastName);
  }

  return (
    <div>
      <h1>Basic Form</h1>
      <form onSubmit={formik.handleSubmit}>
        <div>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            // name="email"
            {...formik.getFieldProps('email')}
          // value={formik.values.email}
          // onChange={formik.handleChange}
          // onBlur={formik.handleBlur}
          />
          {formik.touched.email && formik.errors.email ? formik.errors.email : null}
        </div>

        {/* Aqui hacemos la validacion para mostrar los input. */}

        {formik.values.email.length > 0
          ?
          (
            <>
              <div>
                <label htmlFor="firstName">First Name</label>
                <input
                  id="firstName"
                  type="text"
                  // name="firstName"
                  {...formik.getFieldProps('firstName')}
                // value={formik.values.firstName}
                // onChange={formik.handleChange}
                // onBlur={formik.handleBlur}
                />
                {formik.touched.firstName && formik.errors.firstName ? formik.errors.firstName : null}
              </div>

              <div>
                <label htmlFor="lastName">Last Name</label>
                <input
                  id="lastName"
                  type="text"
                  // name="lastName"
                  {...formik.getFieldProps('lastName')}
                // value={formik.values.lastName}
                // onChange={formik.handleChange}
                // onBlur={formik.handleBlur}
                />
                {formik.touched.lastName && formik.errors.lastName ? formik.errors.lastName : null}
              </div>
            </>
          )
          :
          null}
        <button type="submit">Subscribe</button>
        <button type="reset" onClick={formik.resetForm}>Reset</button>
      </form>
    </div>
  );
}

export default BasicFormValidation;