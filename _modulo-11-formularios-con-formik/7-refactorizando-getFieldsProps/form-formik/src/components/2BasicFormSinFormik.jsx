import { useState } from "react";

const BasicFormSinFormik = () => {

  const [email, setEmail] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');

  function handleSubmit(event) {
    event.preventDefault();

    const form = event.target;
    console.log({
      email: form.email.value,
      firstName: form.firstName.value,
      lastName: form.lastName.value
    });
  }

  return (
    <div>
      <h1>Basic Form</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            name="email"
            value={email}
            onChange={(e) => { setEmail(e.target.value) }} />
        </div>

        {/* Aqui hacemos la validacion para mostrar los input. */}

        {email.length > 0 ? (
          <>
            <div>
              <label htmlFor="firstName">First Name</label>
              <input
                id="firstName"
                type="text"
                name="firstName"
                value={firstName}
                onChange={(e) => { setFirstName(e.target.value) }}
              />
            </div>

            <div>
              <label htmlFor="lastName">Last Name</label>
              <input
                id="lastName"
                type="text"
                name="lastName"
                value={lastName}
                onChange={(e) => { setLastName(e.target.value) }}
              />
            </div>
          </>
        ) : null}
        <button type="submit">Subscribe</button>
      </form>
    </div>
  );
}

export default BasicFormSinFormik;