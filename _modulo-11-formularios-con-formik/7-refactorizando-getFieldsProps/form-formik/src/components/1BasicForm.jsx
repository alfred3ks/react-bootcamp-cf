import { useFormik } from 'formik';

const BasicForm = () => {

  // Creamos nuestro objeto:
  const newsletter = useFormik({
    initialValues: {
      email: '',
      firstName: '',
      lastName: '',
    },
    onSubmit,
  })

  // Funcion onSubmit pasada arriba a newsletter. Se puede pasar directamente alla.
  function onSubmit(values) {
    console.log('valores:', values);
  }

  // function handleSubmit(event) {
  //   // event.preventDefault();
  //   const form = event.target;
  //   console.log({
  //     email: form.email.value,
  //     firstName: form.firstName.value,
  //     lastName: form.lastName.value
  //   });
  // }

  return (
    <div>
      <h1>Basic Form</h1>
      <form onSubmit={newsletter.handleSubmit}>
        <div>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            name="email"
            value={newsletter.values.email}
            onChange={newsletter.handleChange}
          />
        </div>

        <div>
          <label
            htmlFor="firstName">First Name</label>
          <input
            id="firstName"
            type="text"
            name="firstName"
            value={newsletter.values.firstName}
            onChange={newsletter.handleChange}
          />
        </div>
        <div>
          <label htmlFor="lastName">Last Name</label>
          <input
            id="lastName"
            type="text"
            name="lastName"
            value={newsletter.values.lastName}
            onChange={newsletter.handleChange}
          />
        </div>
        <button type="submit">Subscribe</button>
      </form>
    </div>
  );
}

export default BasicForm;

