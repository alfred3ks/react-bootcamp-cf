// import BasicForm from './components/1BasicForm';
// import BasicFormSinFormik from './components/2BasicFormSinFormik';
// import BasicFormConFormik from './components/2BasicFormConFormik';
import BasicFormValidation from './components/3BasicFormValidation';

function App() {
  return (
    <div>
      <BasicFormValidation />
    </div>
  )
}

export default App;
