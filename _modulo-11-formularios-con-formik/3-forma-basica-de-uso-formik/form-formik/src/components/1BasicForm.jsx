const BasicForm = () => {
  function handleSubmit(event) {
    event.preventDefault();

    const form = event.target;
    console.log({
      email: form.email.value,
      firstName: form.firstName.value,
      lastName: form.lastName.value
    });
  }

  return (
    <div>
      <h1>Basic Form</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="email">Email</label>
          <input id="email" type="email" name="email" />
        </div>

        <div>
          <label htmlFor="firstName">First Name</label>
          <input id="firstName" type="text" name="firstName" />
        </div>

        <div>
          <label htmlFor="lastName">Last Name</label>
          <input id="lastName" type="text" name="lastName" />
        </div>
        <button type="submit">Subscribe</button>
      </form>
    </div>
  );
}

export default BasicForm;