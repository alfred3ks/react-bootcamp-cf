/*

Forma básica de Uso:

const formik = useFormik({
  // initialValues: Objeto con la estructura y valor inicial del form
  initialValues:{
    email: '',
    password: '',
  }
  //onSubmit: funcion que recibira el valor final del form.
  onSubmit: (values)=>{}
})

Para usarlo seria asi:
// handleSubmit: funcion definida en onSubmit.
// values: objeto con el estado de los valores del formulario.
// handleChange: actualiza el estado de los valores del form.

<form onSubmit={formik.handleSubmit}{...}>
  <input
  name="email" -> importante para que funcione handChange
  value="{formik.values.email"}
  onChange={formik.handleChange}
  >

</form>

Veamos en el primer ejemplo: formulario para sscribirnos a una pagina, tiene los siguientes inputs:

- email,
- firstName,
- lastName.

form-formik -> creamos en local el proyecto. Luego ves que en el pdf tenemos los enlaces al code sambox.

*/