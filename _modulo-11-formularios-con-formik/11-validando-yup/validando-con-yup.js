/*

Ya hemos visto como iniciar con formik y refactorizar un poco el codigo.

Vamos a hacer ahora una validacion. Lo podemos hacer con una libreria que se llama Yup:
Yup
● Librería open-source
● Permite construir esquemas de validación
● Integración con Formik (recomendada)

const userSchem = object({
  name: string().required(),
  age: number().required().positive().integer(),
  email: string().email(),
})

Esta libreria es muy utilizada a dia de hoy en JS para React.

Debemos instalar esta libreria como dependencia, vamos a su documentacion:
https://github.com/jquense/yup

https://www.npmjs.com/package/yup

npm install -S yup

Vamos a crear un componente llamado 6BasicFormYup.jsx

importamos la dependencia:
import * as Yup from 'yup';

Ahora debemos genera un schema de validacion:

// Creamos el schema de validacion:
const newlettersSchema = Yup.object({
  email: Yup.string().email().required(),
  firstName: Yup.string().required().min(5),
  lastName: Yup.string().required().min(5),
})

Ahora en el atributo del componente Formik le mandamos el schema en validationSchema={newlettersSchema}

<Formik
  initialValues={{ email: '', firstName: '', lastName: '', }}
  onSubmit={handleSubmit}
  validationSchema={newlettersSchema}
>

La funcion de validate la quitamos.

Ahora este manera de validar envia su propio texto de mensaje si no cumple si deseamos colocar el nuestro se lo pasamos a cada funcion required()

const newlettersSchema = Yup.object({
  email: Yup.string().email('Invalid format').required('Required'),
  firstName: Yup.string().required('Required').min(5),
  lastName: Yup.string().required('Required').min(5),
})

*/