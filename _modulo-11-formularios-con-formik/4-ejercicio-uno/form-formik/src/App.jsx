import BasicForm from './components/1BasicForm';

function App() {
  return (
    <div className="App">
      <BasicForm />
    </div>
  )
}

export default App;
