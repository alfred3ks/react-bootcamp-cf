/*

Tenemos nuestro ejercicio usando formik, tenemos que instalar la dependecia:

https://formik.org/

npm install formik --save

Los formularios los ire creando en components/

components/1BasicForm.jsx:

// Creamos nuestro objeto:
const newsletter = useFormik({
  initialValues: {
    email: '',
    firstName: '',
    lastName: '',
  },
  onSubmit:(values)=>{
    console.log(values);
  }
})

y se lo pasamos al metodo onSubmit del form:

return (
  <div>
    <h1>Basic Form</h1>
    <form onSubmit={newsletter.handleSubmit}>
      <div>

Tambien agregamos mas atributos a nuestros input:

import { useFormik } from 'formik';

const BasicForm = () => {

  // Creamos nuestro objeto:
  const newsletter = useFormik({
    initialValues: {
      email: '',
      firstName: '',
      lastName: '',
    },
    onSubmit,
  })

  // Funcion onSubmit pasada arriba a newsletter. Se puede pasar directamente alla.
  function onSubmit(values) {
    console.log('valores:', values);
  }

  function handleSubmit(event) {
    event.preventDefault();

  }

  return (
    <div>
      <h1>Basic Form</h1>
      <form onSubmit={newsletter.handleSubmit}>
        <div>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            name="email"
            value={newsletter.values.email}
            onChange={newsletter.handleChange}
          />
        </div>

        <div>
          <label
            htmlFor="firstName">First Name</label>
          <input
            id="firstName"
            type="text"
            name="firstName"
            value={newsletter.values.firstName}
            onChange={newsletter.handleChange}
          />
        </div>
        <div>
          <label htmlFor="lastName">Last Name</label>
          <input
            id="lastName"
            type="text"
            name="lastName"
            value={newsletter.values.lastName}
            onChange={newsletter.handleChange}
          />
        </div>
        <button type="submit">Subscribe</button>
      </form>
    </div>
  );
}

export default BasicForm;

Este en un ejemplo sencillo como implementar Formik en un formulario. Por lo que veo creamos nuestro objeto usando el hook de formik, luego este objeto ya tiene los metodos hansleSubmit y handleChange, porque por lo que vemos esos metodos no estan definidos y aun asi se ejecutan, seran unos metodos internos del hook.

Nos ahorramos la funcion handleSubmit().

Ahora otra cosa lo que se buscaba con este primer ejercicio era implementar formik en ese ejercicio.

*/