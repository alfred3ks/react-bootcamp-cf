import { Formik, Form, Field, ErrorMessage } from 'formik';

// Aqui creamos la funcion validate para las validaciones: Esta funcion debe retornar un objeto
function validate(values) {
  let errors = {};
  // Validacion del email:
  if (values.email.length === 0) {
    errors.email = 'Email Required';
  } else {
    if (!/[a-z0-9]+@[a-z]+\.[a-z]{2,3}/i.test(values.email)) {
      errors.email = "Invalid format";
    }
  }

  // Validacion del firstName:
  if (values.firstName.length === 0) {
    errors.firstName = 'FirstName required'
  } else if (values.firstName.length <= 5) {
    errors.firstName = 'Should be more than 5 characters'
  }
  // Validacion del lastName:
  if (values.lastName.length === 0) {
    errors.lastName = 'lastName required'
  } else if (values.lastName.length <= 5) {
    errors.lastName = 'Should be more than 5 characters'
  }
  return errors;
}

function handleSubmit(values) {
  console.log(values);
  console.log(values.email, values.firstName, values.lastName);
}

// Aqui tenemos el componente:
const BasicFormFieldComponent = () => {

  return (
    <div>
      <h1>Basic Form</h1>
      {/* Al componente Formik pasamos los atributos del objeto formik que creamos con useFormik */}
      <Formik
        initialValues={{ email: '', firstName: '', lastName: '', }}
        onSubmit={handleSubmit}
        validate={validate}
      >
        {(formik) => (
          // Pasamos el formulario:
          <Form>
            <div>
              <label htmlFor="email">Email</label>
              <Field
                id="email"
                type="email"
                name="email"
              // {...formik.getFieldProps('email')}
              />
              <ErrorMessage name='email' />
            </div>

            {/* Aqui hacemos la validacion para mostrar los input. */}
            {formik.values.email.length > 0
              ?
              (
                <>
                  <div>
                    <label htmlFor="firstName">First Name</label>
                    <Field
                      id="firstName"
                      type="text"
                      name="firstName"
                    // {...formik.getFieldProps('firstName')}
                    />
                    <ErrorMessage name='firstName' />
                  </div>

                  <div>
                    <label htmlFor="lastName">Last Name</label>
                    <Field
                      id="lastName"
                      type="text"
                      name="lastName"
                    // {...formik.getFieldProps('lastName')}
                    />
                    <ErrorMessage name='lastName' />

                  </div>
                </>
              )
              :
              null
            }
            <button type="submit">Subscribe</button>
            <button type="reset">Reset</button>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default BasicFormFieldComponent;