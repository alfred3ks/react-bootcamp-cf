// import BasicForm from './components/1BasicForm';
// import BasicFormSinFormik from './components/2BasicFormSinFormik';
// import BasicFormConFormik from './components/2BasicFormConFormik';
// import BasicFormValidation from './components/3BasicFormValidation';
// import BasicFormComponent from './components/4BasicFormComponent';
import BasicFormFieldComponent from './components/5BasicFormFieldComponent';

function App() {
  return (
    <div>
      <BasicFormFieldComponent />
    </div>
  )
}

export default App;
