/*

Vamos a refactorizar el codigo creado. Vemos que existe mucho codigo que se repite.
Veremos el componente 3BasicFormValidation.jsx

Vamos a ver el componente Formik component:

Funciones y componentes de Formik
● getFieldsProps function
  ○ Reducir props repetido como name, value, onChange, onBlur
● Formik component
  ○ Encierra todo el formulario en un contexto en lugar de usar useFormik
● Form component
  ○ Devuelve una etiqueta form con todos los props de Formik (handleSubmit, handleReset)
● Field component
  ○ Devuelve una etiqueta input con todos los props de la función getFieldProps
● ErrorMessage component
  ○ Agrega la condición cuando input es usado (touched) y si existe el error para mostrar un mensaje

Ahora vamos a ver el componente Field component, creare un nuevo componente:
5BasicFormFieldComponent.jsx

Importamos desde Formik:
import { Formik, Form, Field } from 'formik';

Ahora todos los componentes input vamos a sustituir por Field.
Pero debemos pasarle el atributo a cada field en el caso del email, firstName, lastName y quitar el:
{...formik.getFieldProps('email')}
{...formik.getFieldProps('firstName')}
{...formik.getFieldProps('lastName')}

Por norma general el componente Field renderiza un input pero que pasa si queremos un select o text area pues lo hacemos asi:

<Field as="select"
  id="select"
  type="text"
  name="select"
/>
<Field as="textarea"
  id="textarea"
  type="text"
  name="textarea"
/>

Tambien hemos visto el ErrorMessage:
Lo importamos desde Formik:
Con este componente podemos sustituir todo este codigo de verificacion en los input:
{formik.touched.email && formik.errors.email ? formik.errors.email : null}
{formik.touched.firstName && formik.errors.firstName ? formik.errors.firstName : null}
{formik.touched.lastName && formik.errors.lastName ? formik.errors.lastName : null}

Los vamos a sustituir por:
<ErrorMessage name='email' />
<ErrorMessage name='firstName' />
<ErrorMessage name='lastName' />

Bueno hasta aqui hemos realizado la refactorizacion del codigo.

*/