/*

Formik:

- Libreria opne-source de componentes de React para construir y manejar formularios.
- Ayuda a:
    - Obtener los valores de entrada y de salida de los estados en un formulario,
    - Validaciones y mensaje de error,
    - Manejar envio de formularios.

*/