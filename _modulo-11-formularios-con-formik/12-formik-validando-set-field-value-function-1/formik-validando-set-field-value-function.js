/*

Vamos a ver unas funciones adicionales que tiene Formik.
● setFieldValue function
  ○ Permite actualizar los values del estado del formulario
● FieldArray component
  ○ Ayuda a manejar arrays dentro de los values del form
● useFormikContext component
  ○ Custom hook que te permite acceder a los valor del contexto del componente Formik
Functiones y componentes extra

● setFieldValue function:

Vamos a trabajar con el componente 7FormikSetValueFunction.jsx

Agregaremos un apartado al formulario para invitar amigos.
<div>
  Invited Friends:
</div>

Y Al objeto inicial agregaremos ese valor:
<Formik
    initialValues={{ email: '', firstName: '', lastName: '', invitedFriends: [], }}

Tambien el schema de validacion agregamos su validacion:
const newlettersSchema = Yup.object({
  email: Yup.string().email('Invalid format').required('Required'),
  firstName: Yup.string().required('Required').min(5, 'should have at least 5 caracters'),
  lastName: Yup.string().required('Required').min(5, 'should have at least 5 caracters'),
  invitedFriends: Yup.array(Yup.string().email()),
})

Vemos el componente como ha quedado. Al hacer click se agrega los amigos al arry creado.

Para este ejemplo es mejor buscar ind¡froamcion como usar estos metodos porque no quedaron nada claro.

Mal... muy mal...
Seguimos...




*/