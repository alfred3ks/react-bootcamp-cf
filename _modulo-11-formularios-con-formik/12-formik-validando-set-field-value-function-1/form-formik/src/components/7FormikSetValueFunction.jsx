import { Formik, Form, Field, ErrorMessage } from 'formik';

// Importamos Yup:
import * as Yup from 'yup';

function handleSubmit(values) {
  console.log(values);
  console.log(values.email, values.firstName, values.lastName);
}

// Creamos el schema de validacion:
const newlettersSchema = Yup.object({
  email: Yup.string().email('Invalid format').required('Required'),
  firstName: Yup.string().required('Required').min(5, 'should have at least 5 caracters'),
  lastName: Yup.string().required('Required').min(5, 'should have at least 5 caracters'),
  invitedFriends: Yup.array(Yup.string().email()),
})

// Aqui tenemos el componente:
const FormikSetValueFunction = () => {

  return (
    <div>
      <h1>Basic Form</h1>
      {/* Al componente Formik pasamos los atributos del objeto formik que creamos con useFormik */}
      <Formik
        initialValues={{ email: '', firstName: '', lastName: '', invitedFriends: [], }}
        onSubmit={handleSubmit}
        validationSchema={newlettersSchema}
      >
        {(formik) => (
          <Form>
            <div>
              <label htmlFor="email">Email</label>
              <Field
                id="email"
                type="email"
                name="email"
              />
              <ErrorMessage name='email' />
            </div>

            {/* Aqui hacemos la validacion para mostrar los input. */}
            {formik.values.email.length > 0
              ?
              (
                <>
                  <div>
                    <label htmlFor="firstName">First Name</label>
                    <Field
                      id="firstName"
                      type="text"
                      name="firstName"
                    />
                    <ErrorMessage name='firstName' />
                  </div>

                  <div>
                    <label htmlFor="lastName">Last Name</label>
                    <Field
                      id="lastName"
                      type="text"
                      name="lastName"
                    />
                    <ErrorMessage name='lastName' />
                  </div>
                  <div>Invited Friends:</div>
                  {formik.values.invitedFriends.map((friend, index) => {
                    return (
                      <div key={index}>
                        <Field type="text" name={`invitedFriends[${index}]`} />
                        <ErrorMessage name={`invitedFriends[${index}]`} />
                        <button type='button' onClick={() => { formik.setFieldValue('invitedFriends', formik.values.invitedFriends.filter((subfriend) => { friend !== subfriend })) }}
                        >
                          X
                        </button>
                      </div>
                    )
                  })}
                  <button
                    type='button'
                    onClick={() => {
                      formik.setFieldValue('invitedFriends', [...formik.values.invitedFriends, ''])
                    }}
                  >Add Friend
                  </button>
                </>
              )
              :
              null
            }
            <button type="submit">Subscribe</button>
            <button type="reset">Reset</button>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default FormikSetValueFunction;